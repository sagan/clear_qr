<?php

try
{
session_start();

include "lib_app_constants.php";

$advertiserID = $_SESSION[ADVERTISER_ID];
$sortOrder = $_POST["ddlSortOrder"];

//Check if they are already logged in, else send them back out.
if (!$advertiserID)
{
    //header("Location: http://clearqr.com/login.php");
	header(HTTP_REDIRECT_LOCATION_DOMAIN . "/login.php");
    exit();
}

include "lib_app_account.php";
include "lib_app_qr_ads.php";

//Get the current email address of the logged in advertiser
$advertiserAccountResult = getAdvertiserAccount($advertiserID);
$advertiserAccount = $advertiserAccountResult->objResult;
$email = $advertiserAccount['txt_email'];
$company = $advertiserAccount['txt_company'];

$adAccountResultObj = getAds($advertiserID, $sortOrder);
if (!$adAccountResultObj->bSuccess)
{
    //header("Location: http://clearqr.com/login.php");
	header(HTTP_REDIRECT_LOCATION_DOMAIN . "/login.php");
    exit();
}
else
{
    $adRows = $adAccountResultObj->objResult;

	if (count($adRows) > 0)
	{
		$userMessage = '<b>Reminder: Test your ads after making any changes (scan the QR directly on your screen):</b>';
	}
	else
	{
		$userMessage = 'You currently have no ads, use the create link above to get started...';
	}
}

}
catch (Exception $ex)
{
    include "lib_error_handler.php";
}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - Account Summary</title>
</head>

<body>
<a href="/"><img alt="ClearQR" src="images/clearqr_icon.png"></a>

<hr>

<a href="/">Home</a> > <a href="login.php">Advertising Partner</a> > Account Summary (<?php echo $company ?>)<br><br>

<form method="post" action="account_summary.php">
<table width="100%">
  <tbody>
    <tr>
      <td class="headleft"><a href="ad_new.php">Create</a> a new advertisement.</td>
      <td class="headmiddle"></td>
      <td class="headright"><a href="logout.php">Logout</a> (<?php echo $email ?>)</td>
    </tr>
    <tr>
      <td class="headleft">Sort by: 
					<select name="ddlSortOrder">
						<option value="none"></option>
						<option value="qr">QR#</option>
						<option value="iid">Internal ID</option>
						<option value="dtup">Last Updated</option>
						<option value="stat">Status (active vs inactive)</option>
						<input name="btn_sort" value="Sort" type="submit">
      </td>
      <td class="headmiddle"></td>
      <td class="headright"><a href="edit_profile.php">Edit</a> your account profile</td>
    </tr>
    <tr>
      <td class="headleft"></td>
      <td class="headmiddle"></td>
      <td class="headright">View/Track <a href="account_summary_stats.php">Statistics</a></td>
    </tr>
  </tbody>
</table>
</form>

<?php echo $userMessage; ?><br>

<center>
  <table style="text-align: left;" cellpadding="2" cellspacing="2" border="1">
    <tbody>
      <tr>
        <th ALIGN="CENTER">AD Details</th>
        <th ALIGN="CENTER">Actions</th>
        <th ALIGN="CENTER">QR Image<br>(right click to copy)</th>
      </tr>
<?php
foreach ($adRows as &$row)
{
	$redUrl = $row['txt_url_page'];
	$redUrlHttp = "http://".$redUrl;

	$qrID =  $row['id'];
	$qrImagePath = sprintf('qr_gen_png.php?qr=%d', $qrID);
?>
	<tr>
	<td>
	  <b>QR# </b><?php echo $row['id']; ?><br>
	  <b>Internal ID: </b><?php echo $row['txt_internal_id']; ?><br>
	  <b>Internal Description: </b><?php echo $row['txt_internal_desc']; ?><br>
	  <b>Redirect URL: </b><a href="<?php echo $redUrlHttp; ?>" TARGET="_blank"><?php echo $redUrlHttp; ?></a><br>
	  <b>Public Description: </b><?php echo $row['txt_public_desc']; ?><br>
	  <b>Public Detail: </b><?php echo $row['txt_public_detail']; ?><br>
	  <b>Collect Email Address: </b><?php echo $row['txt_collect_user_email']; ?><br>
	  <b>Last Updated: </b><?php echo $row['dt_update']; ?><br>
	</td>
	<td VALIGN="TOP">
      <a href="ad_edit.php?qr=<?php echo $row['id'] ?>">Edit</a><br><br>
      <a href="ad_view_micro.php?qr=<?php echo $row['id'] ?>" TARGET="_blank">View/Print (micro ad)</a><br>
      <a href="ad_view.php?qr=<?php echo $row['id'] ?>&type=plain" TARGET="_blank">View/Print (plain ad)</a><br>
      <a href="ad_view.php?qr=<?php echo $row['id'] ?>&type=public" TARGET="_blank">View/Print (public ad)</a><br>
      <a href="ad_view.php?qr=<?php echo $row['id'] ?>" TARGET="_blank">View/Print (full info)</a>
	</td>
	<td ALIGN="CENTER">
		<img src="<?php echo $qrImagePath ?>"><br><br>
		QR# </b><?php echo $row['id']; ?><br>
		<?php echo $row['txt_status_cd']; ?>
	</td>
    </tr>
<?php
}
?>
    </tbody>
  </table>
</center>

</body>
</html>
