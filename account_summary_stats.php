<?php

try
{
session_start();

include "lib_app_constants.php";

$advertiserID = $_SESSION[ADVERTISER_ID];
$sortOrder = $_POST["ddlSortOrder"];

//Check if they are already logged in, else send them back out.
if (!$advertiserID)
{
    //header("Location: http://clearqr.com/login.php");
	header(HTTP_REDIRECT_LOCATION_DOMAIN . "/login.php");
    exit();
}

include "lib_app_account.php";
include "lib_app_qr_ads.php";
include "lib_app_stats.php";

//Get the current company/display name of the logged in advertiser
$advertiserAccountResult = getAdvertiserAccount($advertiserID);
$advertiserAccount = $advertiserAccountResult->objResult;
$email = $advertiserAccount['txt_email'];
$company = $advertiserAccount['txt_company'];

$adAccountResultObj = getAds($advertiserID, $sortOrder);
if (!$adAccountResultObj->bSuccess)
{
    //header("Location: http://clearqr.com/login.php");
	header(HTTP_REDIRECT_LOCATION_DOMAIN . "/login.php");
    exit();
}
else
{
    $adRows = $adAccountResultObj->objResult;

	if (count($adRows) > 0)
	{
		$userMessage = '<b>Here are your ad tracking statistics:</b>';

		//Get the stats from the DB
		$adCountResultObj = getCountsByAd($advertiserID);
		$adCountHashTable = $adCountResultObj->objResult;
	}
	else
	{
		$userMessage = 'You currently have no ads...';
	}
}

}
catch (Exception $ex)
{
    include "lib_error_handler.php";
}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - Ad Statistics</title>
</head>

<body>
<a href="/"><img alt="ClearQR" src="images/clearqr_icon.png"></a>

<hr>

<a href="/">Home</a> > <a href="login.php">Advertising Partner</a> > Ad Statistics (<?php echo $company ?>)<br><br>

<form method="post" action="account_summary_stats.php">
<table width="100%">
  <tbody>
    <tr>
      <td class="headleft">Sort by: 
					<select name="ddlSortOrder">
						<option value="none"></option>
						<option value="qr">QR#</option>
						<option value="iid">Internal ID</option>
						<option value="dtup">Last Updated</option>
						<option value="stat">Status (active vs inactive)</option>
						<input name="btn_sort" value="Sort" type="submit">
      </td>
      <td class="headmiddle"></td>
      <td class="headright"><a href="account_summary.php">Back</a> to your account summary</td>
    </tr>
  </tbody>
</table>
</form>

<?php echo $userMessage; ?><br>

<center>
  <table style="text-align: left;" cellpadding="2" cellspacing="2" border="1">
    <tbody>
      <tr>
        <th ALIGN="CENTER">QR#</th>
        <th ALIGN="CENTER">Internal ID</th>
        <th ALIGN="CENTER">Internal Description</th>
        <th ALIGN="CENTER">Last Updated</th>
        <th ALIGN="CENTER">Status</th>
        <th ALIGN="CENTER">Views</th>
        <th ALIGN="CENTER">Last Viewed</th>
      </tr>
<?php
foreach ($adRows as &$row)
{
	$redUrl = $row['txt_url_page'];
	$redUrlHttp = "http://".$redUrl;

	$qrID =  $row['id'];
	$qrImagePath = sprintf('qr_gen_png.php?qr=%d', $qrID);

	$adCountRow = $adCountHashTable[$qrID];
?>
	<tr>
	  <td><a href="<?php echo $redUrlHttp; ?>" TARGET="_blank"><?php echo $row['id']; ?></a></td>
	  <td><?php echo $row['txt_internal_id']; ?></td>
	  <td><?php echo $row['txt_internal_desc']; ?></td>
	  <td><?php echo $row['dt_update']; ?></td>
	  <td><?php echo $row['txt_status_cd']; ?></td>
	  <td><?php echo $adCountRow['num_view_count']; ?></td>
	  <td><?php echo $adCountRow['dt_view_last']; ?></td>
    </tr>
<?php
}
?>
    </tbody>
  </table>
</center>

</body>
</html>
