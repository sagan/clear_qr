<?php

try
{
session_start();

include "lib_app_constants.php";

$btn_save = $_POST["btn_save_qr_ad"];
$advertiserID = $_SESSION[ADVERTISER_ID];
$qrID = trim($_GET["qr"]);

//Check if they are already logged in, else send them back out.
if (!$advertiserID)
{
    //header("Location: http://clearqr.com/login.php");
	header(HTTP_REDIRECT_LOCATION_DOMAIN . "/login.php");
    exit();
}

include "lib_app_qr_ads.php";

$adResultObj = getAd($qrID, $advertiserID);
if (!$adResultObj->bSuccess)
{
    //header("Location: http://clearqr.com/account_summary.php");
	header(HTTP_REDIRECT_LOCATION_DOMAIN . "/account_summary.php");
    exit();
}
else
{
    $adDataRow = $adResultObj->objResult;
    $redirect_url = $adDataRow['txt_url_page'];
    $internal_id = $adDataRow['txt_internal_id'];
    $internal_desc = $adDataRow['txt_internal_desc'];
    $public_desc= $adDataRow['txt_public_desc'];
    $public_detail = $adDataRow['txt_public_detail'];
    $collect_email = $adDataRow['txt_collect_user_email'];
    $status_cd = $adDataRow['txt_status_cd'];
}

if ($btn_save)
{
	$redirect_url = trim($_POST["redirect_url"]);
	$internal_id = trim($_POST["internal_id"]);
	$internal_desc = trim($_POST["internal_desc"]);
	$public_desc = trim($_POST["public_desc"]);
	$public_detail = trim($_POST["public_detail"]);
	$collect_email = trim($_POST["ddl_collect_email"]);
	$status_cd = trim($_POST["ddl_status"]);

	$iReplaceCount = 1;
    $redirect_url = str_replace("http://", "", $redirect_url, $iReplaceCount);
    $redirect_url = str_replace(" ", "", $redirect_url);

	$editAdResultObj = editExistingQrAd($qrID, $advertiserID, $redirect_url, $internal_id, $internal_desc, $public_desc, $public_detail, $collect_email, $status_cd);
	if ($editAdResultObj->bSuccess)
	{
	    //header("Location: http://clearqr.com/ad_edit_done.php");
		header(HTTP_REDIRECT_LOCATION_DOMAIN . "/ad_edit_done.php");
	    exit();
	}
	else
	{
		$error_tag = $editAdResultObj->exStr;
	}
}

	//COLLECT_EMAIL: Determine which DDL option to set
	if ($collect_email == AD_COLLECT_EMAIL_OPTIONAL)
	{
		$ddlOptionalSelected = "SELECTED";
	}
	else if ($collect_email == AD_COLLECT_EMAIL_REQUIRE)
	{
		$ddlRequireSelected = "SELECTED";
	}
	else if ($collect_email == AD_COLLECT_EMAIL_DONT_ASK)
	{
		$ddlDontAskSelected = "SELECTED";
	}

	//STATUS_CD: Determne which DDL option to set
	if ($status_cd == AD_ACTIVE)
	{
		$ddlStatusActiveSelected = "SELECTED";
	}
	else if ($status_cd == AD_INACTIVE)
	{
		$ddlStatusInactiveSelected = "SELECTED";
	}

}
catch (Exception $ex)
{
    include "lib_error_handler.php";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - Edit Ad</title>
</head>

<body>
<a href="/"><img alt="ClearQR" src="images/clearqr_icon.png"></a>

<hr>

<a href="/">Home</a> > <a href="login.php">Advertising Partner</a> > Edit Ad

<form method="post" action="ad_edit.php?qr=<?php echo $qrID; ?>">
  <center>
  <table style="text-align: left;" cellpadding="2" cellspacing="5">
    <tbody>
      <tr>
        <td class="tableft"></td>
		<td class="tabmiddle" colspan="2"><span class="errortxt"><?PHP echo $error_tag; ?></span></td>
      </tr>
      <tr>
        <td class="tableft">QR#:</td>
        <td class="tabmiddle"><?php echo $qrID; ?></td>
        <td class="tabright"></td>
      </tr>
      <tr>
        <td class="tableft">Internal ID:</td>
        <td class="tabmiddle"><input size=50 name="internal_id" value="<?php echo $internal_id; ?>"></td>
        <td class="tabright">This is an ID for how you lookup the item (i.e. MLS#, VIN#, SKU#, etc).</td>
      </tr>
      <tr>
        <td class="tableft">Internal Description:</td>
        <td class="tabmiddle"><input size=50 name="internal_desc" value="<?php echo $internal_desc; ?>"></td>
        <td class="tabright">How you reference the item with your employees.</td>
      </tr>
      <tr>
        <td class="tableft">Redirect URL (http://):</td>
        <td class="tabmiddle"><input size=50 name="redirect_url" value="<?php echo $redirect_url; ?>"></td>
        <td class="tabright">This is the web page you want people to see.</td>
      </tr>
      <tr>
        <td class="tableft">Public Ad Description:</td>
        <td class="tabmiddle"><input size=50 name="public_desc" value="<?php echo $public_desc; ?>"></td>
        <td class="tabright">How your customers describe the item (i.e. Street Address, Car Name, Product Type, etc).</td>
      </tr>
      <tr>
        <td class="tableft">Public Ad Detail:</td>
        <td class="tabmiddle"><input size=50 name="public_detail" value="<?php echo $public_detail; ?>"></td>
        <td class="tabright">In case it helps to distinguish the item (Home Type, Car Color, etc).</td>
      </tr>
      <tr>
        <td class="tableft">Collect User Email:</td>
        <td class="tabmiddle"><select name="ddl_collect_email">
					<option <?php echo $ddlOptionalSelected; ?> value="OPTIONAL">Optional Email</option>
					<option <?php echo $ddlRequireSelected; ?> value="REQUIRE">Require Email</option>
					<option <?php echo $ddlDontAskSelected; ?> value="DONT_ASK">Dont ask, just show the ad</option>
							</select></td>
        <td class="tabright">When users initiate your ad, do you want to collect their email?</td>
      </tr>
      <tr>
        <td class="tableft">Status:</td>
        <td class="tabmiddle"><select name="ddl_status">
					<option <?php echo $ddlStatusActiveSelected; ?> value="ACTIVE">ACTIVE</option>
					<option <?php echo $ddlStatusInactiveSelected; ?> value="INACTIVE">INACTIVE</option>
							</select></td>
        <td class="tabright">Inactive ADs will forward users to the Default-URL in your account profile.</td>
      </tr>
      <tr>
        <td class="tableft"></td>
        <td class="tabmiddle"><input name="btn_save_qr_ad" value="Save" type="submit"></td>
        <td class="tabright"></td>
      </tr>
    </tbody>
  </table>
  </center>
</form>

<br><br>

<center>
<table cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td class="tableft"></td>
      <td class="tabmiddle"><i>Note: Make sure to test your AD after you edit it.</i></td>
      <td class="tabright"></td>
    </tr>
  </tbody>
</table>
</center>

</body>
</html>
