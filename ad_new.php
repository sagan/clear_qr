<?php

try
{
session_start();

include "lib_app_constants.php";

$btn_save = $_POST["btn_save_qr_ad"];
$advertiserID = $_SESSION[ADVERTISER_ID];

//Check if they are already logged in, else send them back out.
if (!$advertiserID)
{
    //header("Location: http://clearqr.com/login.php");
	header(HTTP_REDIRECT_LOCATION_DOMAIN . "/login.php");
    exit();
}

if ($btn_save)
{
	include "lib_app_qr_ads.php";
	$redirect_url = trim($_POST["redirect_url"]);
	$internal_id = trim($_POST["internal_id"]);
	$internal_desc = trim($_POST["internal_desc"]);
	$public_desc = trim($_POST["public_desc"]);
	$public_detail = trim($_POST["public_detail"]);
	$collect_email = trim($_POST["ddlCollectUserEmail"]);

	$iReplaceCount = 1;
    $redirect_url = str_replace("http://", "", $redirect_url, $iReplaceCount);
    $redirect_url = str_replace(" ", "", $redirect_url);

	$createAdResultObj = createNewQrAd($advertiserID, $redirect_url, $internal_id, $internal_desc, $public_desc, $public_detail, $collect_email);
	if ($createAdResultObj->bSuccess)
	{
	    //header("Location: http://clearqr.com/ad_new_done.php");
		header(HTTP_REDIRECT_LOCATION_DOMAIN . "/ad_new_done.php");
	    exit();
	}
	else
	{
		$error_tag = $createAdResultObj->exStr;
	}
}

    //Determine which DDL option to default
    if ($collect_email == AD_COLLECT_EMAIL_OPTIONAL)
    {
        $ddlOptionalSelected = "SELECTED";
    }
    else if ($collect_email == AD_COLLECT_EMAIL_REQUIRE)
    {
        $ddlRequireSelected = "SELECTED";
    }
    else if ($collect_email == AD_COLLECT_EMAIL_DONT_ASK)
    {
        $ddlDontAskSelected = "SELECTED";
    }

}
catch (Exception $ex)
{
    include "lib_error_handler.php";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - Create Ad</title>
</head>

<body>
<a href="/"><img alt="ClearQR" src="images/clearqr_icon.png"></a>

<hr>

<a href="/">Home</a> > <a href="login.php">Advertising Partner</a> > Create Ad

<form method="post" action="ad_new.php">
  <center>
  <table style="text-align: left;" cellpadding="2" cellspacing="5">
    <tbody>
      <tr>
        <td class="tableft"></td>
		<td class="tabmiddle" colspan="2"><span class="errortxt"><?PHP echo $error_tag; ?></span></td>
      </tr>
      <tr>
        <td class="tableft">QR#:</td>
        <td class="tabmiddle">Create New QR AD</td>
        <td class="tabright"></td>
      </tr>
      <tr>
        <td class="tableft">Internal ID:</td>
        <td class="tabmiddle"><input size=50 name="internal_id" value="<?php echo $internal_id; ?>"></td>
        <td class="tabright">This is an ID for how you lookup the item (i.e. MLS#, VIN#, SKU#, etc).</td>
      </tr>
      <tr>
        <td class="tableft">Internal Description:</td>
        <td class="tabmiddle"><input size=50 name="internal_desc" value="<?php echo $internal_desc; ?>"></td>
        <td class="tabright">How you reference the item with your employees.</td>
      </tr>
      <tr>
        <td class="tableft">Redirect URL (http://):</td>
        <td class="tabmiddle"><input size=50 name="redirect_url" value="<?php echo $redirect_url; ?>"></td>
        <td class="tabright">This is the web page you want people to see.</td>
      </tr>
      <tr>
        <td class="tableft">Public Ad Description:</td>
        <td class="tabmiddle"><input size=50 name="public_desc" value="<?php echo $public_desc; ?>"></td>
        <td class="tabright">How your customers describe the item (i.e. Street Address, Car Name, Product Type, etc)</td>
      </tr>
      <tr>
        <td class="tableft">Public Ad Detail:</td>
        <td class="tabmiddle"><input size=50 name="public_detail" value="<?php echo $public_detail; ?>"></td>
        <td class="tabright">In case it helps to distinguish the item (Home Type, Car Color, etc).</td>
      </tr>
      <tr>
        <td class="tableft">Collect User Email:</td>
        <td class="tabmiddle"><select name="ddlCollectUserEmail">
					<option <?php echo $ddlOptionalSelected; ?> value="OPTIONAL">Optional Email</option>
					<option <?php echo $ddlRequireSelected; ?> value="REQUIRE">Require Email</option>
					<option <?php echo $ddlDontAskSelected; ?> value="DONT_ASK">Dont ask, just show the ad</option>
							</select></td>
        <td class="tabright">When users initiate your ad, do you want to collect their email?</td>
      </tr>
      <tr>
        <td class="tableft"></td>
        <td class="tabmiddle"><input name="btn_save_qr_ad" value="Save" type="submit"></td>
        <td class="tabright"></td>
      </tr>
    </tbody>
  </table>
  </center>
</form>

<br><br>

<center>
<table cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td class="tableft"></td>
      <td class="tabmiddle"><i>Note: Make sure to test your AD after you create it.</i></td>
      <td class="tabright"></td>
    </tr>
  </tbody>
</table>
</center>

</body>
</html>
