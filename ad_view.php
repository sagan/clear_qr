<?php

try
{
session_start();

include "lib_app_constants.php";

$advertiserID = $_SESSION[ADVERTISER_ID];
$qrID = trim($_GET["qr"]);
$adType = trim($_GET["type"]);

//Check if they are already logged in, else send them back out.
if (!$advertiserID)
{
    //header("Location: http://clearqr.com/login.php");
	header(HTTP_REDIRECT_LOCATION_DOMAIN . "/login.php");
    exit();
}

include "lib_app_account.php";
include "lib_app_qr_ads.php";

$adResultObj = getAd($qrID, $advertiserID);
if (!$adResultObj->bSuccess)
{
    //header("Location: http://clearqr.com/account_summary.php");
	header(HTTP_REDIRECT_LOCATION_DOMAIN . "/account_summary.php");
    exit();
}
else
{
    $adDataRow = $adResultObj->objResult;
    $redirect_url = $adDataRow['txt_url_page'];
    $internal_id = $adDataRow['txt_internal_id'];
    $internal_desc = $adDataRow['txt_internal_desc'];
    $public_desc= $adDataRow['txt_public_desc'];
    $public_detail = $adDataRow['txt_public_detail'];
    $collect_email = $adDataRow['txt_collect_user_email'];
    $status_cd = $adDataRow['txt_status_cd'];

	//Get the current CompanyName of the logged in advertiser
	$advertiserAccountResult = getAdvertiserAccount($advertiserID);
	$advertiserAccount = $advertiserAccountResult->objResult;
	$company = $advertiserAccount['txt_company'];
	$logo_icon_url = $advertiserAccount['txt_logo_icon_url'];
	$logo_icon_url_http = "http://" . $logo_icon_url;

	if ($logo_icon_url == "")
	{
		$logo_title = 
		$logo_title = "<h1>$company</h1>";
	}
	else
	{
		$logo_title = '<img alt="Logo Icon URL" src="' . $logo_icon_url_http . '">';
	}
}

}
catch (Exception $ex)
{
	include "lib_error_handler.php";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - View / Print Ad</title>
</head>

<body>

<table width="100%">
  <tbody>
    <tr>
      <td class="headleft"><?php echo $logo_title; ?></td>
      <td class="headmiddle"></td>
      <td class="headright"><b><i>Powered by</i></b><br>
	<img alt="ClearQR" src="images/clearqr_icon.png"></td>
    </tr>
  </tbody>
</table>

<hr>

<center>
  <table style="text-align: left;" cellpadding="2" cellspacing="2">
    <tbody>
      <tr>
        <td class="centerleft"></td>
		<td class="centermiddle">Use your mobile phone to scan the QR Barcode below:</td>
        <td class="centerright"></td>
      </tr>
      <tr>
        <td class="centerleft"></td>
		<td class="centermiddle"><img src="qr_gen_png.php?qr=<?php echo $qrID; ?>"></td>
        <td class="centerright"></td>
      </tr>
      <tr>
        <td class="centerleft"></td>
        <td class="centermiddle">(Dont have a QR scanner on your phone? Get it free at <?php echo HTTP_DOMAIN; ?>)</td>
        <td class="centerright"></td>
      </tr>
      <tr>
        <td class="centerleft">QR#:</td>
        <td class="centermiddle"><b><?php echo $qrID; ?></b></td>
        <td class="centerright"></td>
      </tr>
<?php
if ($adType != 'public' && $adType != 'plain')
{
?>
      <tr>
        <td class="centerleft">Internal ID:</td>
        <td class="centermiddle"><b><?php echo $internal_id; ?></b></td>
        <td class="centerright"></td>
      </tr>
      <tr>
        <td class="centerleft">Internal Description:</td>
        <td class="centermiddle"><b><?php echo $internal_desc; ?></b></td>
        <td class="centerright"></td>
      </tr>
<?php
}
?>
      <tr>
        <td class="centerleft">Advertiser:</td>
        <td class="centermiddle"><b><?php echo $company; ?></b></td>
        <td class="centerright"></td>
      </tr>
<?php
if ($adType != 'plain')
{
?>
      <tr>
        <td class="centerleft">Ad Description:</td>
        <td class="centermiddle"><b><?php echo $public_desc; ?></b></td>
        <td class="centerright"></td>
      </tr>
      <tr>
        <td class="centerleft">Ad Detail:</td>
        <td class="centermiddle"><b><?php echo $public_detail; ?></b></td>
        <td class="centerright"></td>
      </tr>
      <tr>
        <td class="centerleft">Additional Info:</td>
        <td class="centermiddle"><textarea rows="10" cols="40"></textarea></td>
        <td class="centerright"></td>
      </tr>
<?php
}
?>
    </tbody>
  </table>
</center>

</body>
</html>
