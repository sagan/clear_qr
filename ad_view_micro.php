<?php

try
{
session_start();

include "lib_app_constants.php";

$qrID = trim($_GET["qr"]);

}
catch (Exception $ex)
{
	include "lib_error_handler.php";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - View / Print Ad</title>
</head>

<body>

<table style="text-align: center;">
<tr><td>QR#: <?php echo $qrID; ?></td></tr>
<tr><td><img src="qr_gen_png.php?qr=<?php echo $qrID; ?>"></td></tr>
<tr><td>No QR scanner?<br>Goto <?php echo HTTP_DOMAIN; ?></td></tr>
</table>


</body>
</html>
