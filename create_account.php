<?PHP

try
{
session_start();

include "lib_app_constants.php";

$btnClickAgreeTOS = $_POST["btn_agree_tos"];
$btnClickCreateAccount = $_POST["btn_submit_create_account"];
$bViewedTOS = $_SESSION[TERM_OF_SERVICE_VIEWED];

if (($btnClickAgreeTOS || $btnClickCreateAccount) && $bViewedTOS)
{
	//Good, either you are coming from TOS page, or posting back
}
else
{
	//header("Location: http://clearqr.com/login.php");
	header(HTTP_REDIRECT_LOCATION_DOMAIN . "/login.php");
	exit();
}

if ($btnClickCreateAccount)
{
	include "lib_app_account.php";
	$email = trim($_POST["email"]);
	$email2 = trim($_POST["email_verify"]);
	$passwd = $_POST["passwd"];
	$passwd2 = $_POST["passwd_verify"];
	$company = trim($_POST["company"]);
	$contact = trim($_POST["contact"]);
	$phone_num = trim($_POST["phone_num"]);
	$default_url = trim($_POST["default_url"]);
	$logo_icon_url = trim($_POST["logo_icon_url"]);

	$iReplaceCountDefaultURL = 1;
	$default_url = str_replace("http://", "", $default_url, $iReplaceCountDefaultURL); 
	$default_url = str_replace(" ", "", $default_url); 

	$iReplaceCountLogoIconURL = 1;
	$logo_icon_url = str_replace("http://", "", $logo_icon_url, $iReplaceCountLogoIconURL); 
	$logo_icon_url = str_replace(" ", "", $logo_icon_url); 

	$email = strtolower($email);
	$email2 = strtolower($email2);
	$phone_num = str_replace(" ", "", $phone_num);

	$resObj = createNewAccount($email, $email2, $passwd, $passwd2, $company, $contact, $phone_num, $default_url, $logo_icon_url);
	if ($resObj->bSuccess)
	{
		$_SESSION[ADVERTISER_ID] = $resObj->objResult;
		//header("Location: http://clearqr.com/create_account_done.php");
		header(HTTP_REDIRECT_LOCATION_DOMAIN . "/create_account_done.php");
		exit();
	}
	else
	{
		$error_tag = $resObj->exStr;
		if ($resObj->exCode == EMAIL_IN_USE)
		{
			$error_tag = 'Email address already exists, <a href="login.php">login</a> instead.';
		}
	}
}

}
catch (Exception $ex)
{
	include "lib_error_handler.php";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - Create Account</title>
</head>

<body>
<a href="/"><img alt="ClearQR" src="images/clearqr_icon.png"></a>

<hr>

<a href="/">Home</a> > <a href="login.php">Advertising Partner</a> > Create Account

<form method="post" action="create_account.php">
  <center>
  <table style="text-align: left;" cellpadding="2" cellspacing="5">
    <tbody>
      <tr>
        <td class="tableft"></td>
        <td class="tabmiddle" colspan="2"><span class="errortxt"><?PHP echo $error_tag; ?></span></td>
      </tr>
      <tr>
        <td class="tableft">Email:</td>
        <td class="tabmiddle"><input size=30 name="email" value="<?PHP echo $email; ?>"></td>
        <td class="tabright">Example: you@gmail.com</td>
      </tr>
      <tr>
        <td class="tableft">Email (verify):</td>
        <td class="tabmiddle"><input size=30 name="email_verify" value="<?PHP echo $email2; ?>"></td>
        <td class="tabright"></td>
      </tr>
      <tr>
        <td class="tableft">Password:</td>
        <td class="tabmiddle"><input name="passwd" type="password"></td>
        <td class="tabright">Minimum 6 characters</td>
      </tr>
      <tr>
        <td class="tableft">Password (verify):</td>
        <td class="tabmiddle"><input name="passwd_verify" type="password"></td>
        <td class="tabright"></td>
      </tr>
      <tr>
        <td class="tableft">Company / Display Name:</td>
        <td class="tabmiddle"><input size=30 name="company" value="<?PHP echo $company; ?>"></td>
        <td class="tabright">This name will be shown in your advertisements.</td>
      </tr>
      <tr>
        <td class="tableft">Default URL (http://)</td>
        <td class="tabmiddle"><input size=30 name="default_url" value="<?PHP echo $default_url; ?>"></td>
        <td class="tabright">Web page to show a customer when an AD has become deactive (i.e. your home page).</td>
      </tr>
      <tr>
        <td class="tableft">Logo/Icon URL (http://)</td>
        <td class="tabmiddle"><input size=30 name="logo_icon_url" value="<?PHP echo $logo_icon_url; ?>"></td>
        <td class="tabright">Logo/Icon to display in an AD (i.e. jpg, gif, png file).
							Find it on your web site, right-click, copy the URL, and paste it here.</td>
      </tr>
      <tr>
        <td class="tableft">Contact Name:</td>
        <td class="tabmiddle"><input size=30 name="contact" value="<?PHP echo $contact; ?>"></td>
        <td class="tabright"></td>
      </tr>
      <tr>
        <td class="tableft">Phone #:</td>
        <td class="tabmiddle"><input size=30 name="phone_num" value="<?PHP echo $phone_num; ?>"></td>
        <td class="tabright">Format allows only numbers, and + or - characters</td>
      </tr>
      <tr>
        <td class="tableft"></td>
        <td class="tabmiddle"><input name="btn_submit_create_account" value="Submit" type="submit"></td>
        <td class="tabright"></td>
      </tr>
    </tbody>
  </table>
  </center>
</form>

<br><br>

<center>
<table cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td class="tableft"></td>
      <td class="tabmiddle"><i>Send all inquiries to: <?php echo EMAIL_ADMIN; ?></i></td>
      <td class="tabright"></td>
    </tr>
  </tbody>
</table>
</center>

</body>
</html>
