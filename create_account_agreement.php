<?PHP

try
{
session_start();
session_unset();

include "lib_app_constants.php";

$_SESSION[TERM_OF_SERVICE_VIEWED] = 1;

//$session_guard = uniqid();
//$_SESSION["SESSION_GUARD"] = $session_guard;
//<input type="hidden" name="session_guard" value="$MY_PHP_VAL" />
}
catch (Exception $ex)
{
    include "lib_error_handler.php";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - Terms of Service</title>
</head>

<body>
<a href="/"><img alt="ClearQR" src="images/clearqr_icon.png"></a>

<hr>

<a href="/">Home</a> > <a href="login.php">Advertising Partner</a> > Terms of Service<br><br>

As part of your new account registration, please read and agree to the following Terms of Service:<br><br>

<b>Q) Does ClearQR have any responsibility to its Advertising Partners?</b>
<br>
A) No, as partners you are allowed to use the service, with no guarantee/warranty of service or uptime. ClearQR reserves the right to disable or cancel your account at any time, for any reason, without any warning or prior notice.
<br><br>

<b>Q) What is the relationship between ClearQR and its Advertising Partners?</b>
<br>
A) There is no legal relationship.
<br><br>

<b>Q) What are my rights?</b>
<br>
A) You have none; ClearQR can terminate your account at anytime.  This situation would typically be due to using the system in a malicious, potentially harmful, or illegal way.
<br><br>

<b>Q) Who owns the data?</b>
<br>
A) ClearQR owns all the data collected.
<br><br>

<b>Q) Will the service always be free?</b>
<br>
A) The service is currently in a trial mode to measure its potential for both advertisers and users.  Depending on the success of this trial, access to specialized data reports may become fee based in the future.  Any changes to service usage fees, or the Terms of Service, are solely based on the discretion of ClearQR; these changes can occur without prior notice.
<br><br>

<b>Q) What obligations or liability does ClearQR posses?</b>
<br>
A) None.  In no event shall ClearQR be liable for any direct, indirect, incidental, special, or consequential damages, resulting from the use or inability to use the service; including but not limited to damages from loss of profits, even if ClearQR has been advised of the possibility of such damages.
<br><br>

<b>Q) Do you agree to indemnify ClearQR?</b>
<br>
A) Yes.  You hereby agree, at Your expense, to indemnify, defend and hold ClearQR harmless from and against any loss, cost, damages, liability or expense arising out of or relating to (a) a third-party claim, action or allegation of infringement based on information, data, files or other content submitted by You; (b) any fraud or manipulation, or other breach of these Terms by You; or (c) any third-party claim, action or allegation brought against ClearQR arising out of or relating to a dispute between its users over the terms and conditions of a contract or related to the purchase and sale of any services.
<br><br>

<b>Q) Again, what obligations or liability does ClearQR posses?</b>
<br>
A) None, you agree to release all liability by accepting this agreement..
<br><br>

<form method="post" action="create_account.php">
  <center>
  <input value="I Agree, proceed to create my free account" name= "btn_agree_tos" type="submit">
  </center>
</form>

<i>Send all inquiries to: <?php echo EMAIL_ADMIN; ?></i>

</body>
</html>
