<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - Test</title>
</head>

<body>
<a href="/"><img alt="ClearQR" src="images/clearqr_icon.png"></a>

<hr>

<a href="/">Home</a> > Test

<br><br>
Here is a test:
<br><br>

<table border="1">
<?php

include "lib_app_constants.php";
include "lib_app_test.php";

try
{
	$insertResultObj = insertTestRow();
	$rowCountInserted = $insertResultObj->objResult;

	$selectResultObj = getTestRows();
	$dataArray = $selectResultObj->objResult;
}
catch (Exception $ex)
{
	include "lib_error_handler.php"; 
}

echo "RowCountInserted=".$rowCountInserted."<br><br>";

echo "<tr><th>id</th><th>a</th><th>b</th><th>c</th><th>c-sec</th></tr>";
foreach ($dataArray as &$row) 
{
	//http://www.php.net/manual/en/function.date-parse.php
	$hashDate = date_parse($row['dt_c']);

	echo "<tr>";
	echo "<td>";
	echo $row['id'];
	echo "</td>";
	echo "<td>";
	echo $row['str_a'];
	echo "</td>";
	echo "<td>";
	echo $row['num_b'];
	echo "</td>";
	echo "<td>";
	echo $row['dt_c'];
	echo "</td>";
	echo "<td>";
	echo $hashDate['second'];
	echo "</td>";
	echo "</tr>";
}

?>

</table>
<br><br>
All Done!
</body>
</html>
