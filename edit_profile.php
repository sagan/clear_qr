<?PHP

try
{
session_start();

include "lib_app_constants.php";

$btnClickSaveProfile = $_POST["btn_submit_save_profile"];
$btnClickSavePasswd = $_POST["btn_submit_save_passwd"];
$advertiserID = $_SESSION[ADVERTISER_ID];

//Check if they are already logged in, else send them back out.
if (!$advertiserID)
{
	//header("Location: http://clearqr.com/login.php");
	header(HTTP_REDIRECT_LOCATION_DOMAIN . "/login.php");
	exit();
}

include "lib_app_account.php";

$adAccountResultObj = getAdvertiserAccount($advertiserID);
if (!$adAccountResultObj->bSuccess)
{
	//header("Location: http://clearqr.com/login.php");
	header(HTTP_REDIRECT_LOCATION_DOMAIN . "/login.php");
	exit();
}
else
{
	$adAccountDataRow = $adAccountResultObj->objResult;
	$email = $adAccountDataRow['txt_email'];
	$company = $adAccountDataRow['txt_company'];
	$contact = $adAccountDataRow['txt_contact_name'];
	$phone_num = $adAccountDataRow['txt_phone_number'];
	$default_url = $adAccountDataRow['txt_default_url'];
	$logo_icon_url = $adAccountDataRow['txt_logo_icon_url'];
}

if ($btnClickSaveProfile)
{
	$company = trim($_POST["company"]);
	$contact = trim($_POST["contact"]);
	$phone_num = trim($_POST["phone_num"]);
	$default_url = trim($_POST["default_url"]);
	$logo_icon_url = trim($_POST["logo_icon_url"]);

    $iReplaceCountDefaultURL = 1;
    $default_url = str_replace("http://", "", $default_url, $iReplaceCountDefaultURL);
    $default_url = str_replace(" ", "", $default_url);

    $iReplaceCountLogoIconURL = 1;
    $logo_icon_url = str_replace("http://", "", $logo_icon_url, $iReplaceCountLogoIconURL);
    $logo_icon_url = str_replace(" ", "", $logo_icon_url);

	$phone_num = str_replace(" ", "", $phone_num);

	$resObj = accountEditProfile($advertiserID, $email, $company, $contact, $phone_num, $default_url, $logo_icon_url);
	if ($resObj->bSuccess)
	{
		//header("Location: http://clearqr.com/edit_profile_done.php");
		header(HTTP_REDIRECT_LOCATION_DOMAIN . "/edit_profile_done.php");
		exit();
	}
	else
	{
		$error_tag = $resObj->exStr;
	}
}
else if ($btnClickSavePasswd)
{
	$passwd = $_POST["passwd"];
	$passwd2 = $_POST["passwd_verify"];

    $resObj = accountChangePasssword($advertiserID, $email, $passwd, $passwd2);
    if ($resObj->bSuccess)
    {
		$error_tag = "Your password has been changed successfully";
    }
    else
    {
        $error_tag = $resObj->exStr;
    }

}

}
catch (Exception $ex)
{
	include "lib_error_handler.php";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - Edit Profile</title>
</head>

<body>
<a href="/"><img alt="ClearQR" src="images/clearqr_icon.png"></a>

<hr>

<a href="/">Home</a> > <a href="login.php">Advertising Partner</a> > Edit Profile

<form method="post" action="edit_profile.php">
  <center>
  <table style="text-align: left;" cellpadding="2" cellspacing="5">
    <tbody>
      <tr>
        <td class="tableft"></td>
        <td class="tabmiddle" colspan="2"><span class="errortxt"><?PHP echo $error_tag; ?></span></td>
      </tr>
      <tr>
        <td class="tableft">Email:</td>
        <td class="tabmiddle"><b><?PHP echo $email ?></b></td>
        <td class="tabright"></td>
      </tr>
      <tr>
        <td class="tableft">Password:</td>
        <td class="tabmiddle"><input name="passwd" type="password"></td>
        <td class="tabright">Minimum 6 characters</td>
      </tr>
      <tr>
        <td class="tableft">Password (verify):</td>
        <td class="tabmiddle"><input name="passwd_verify" type="password"></td>
        <td class="tabright"></td>
      </tr>
      <tr>
        <td class="tableft"></td>
        <td class="tabmiddle"><input name="btn_submit_save_passwd" value="Change Password" type="submit"></td>
        <td class="tabright"></td>
      </tr>
      <tr>
        <td class="tableft"></td>
        <td class="tabmiddle"><b>- OR -</b></td>
        <td class="tabright"></td>
      </tr>
      <tr>
        <td class="tableft">Company / Display Name:</td>
        <td class="tabmiddle"><input size=30 name="company" value="<?PHP echo $company; ?>"></td>
        <td class="tabright">This name will be shown in your advertisements.</td>
      </tr>
      <tr>
        <td class="tableft">Default URL (http://)</td>
        <td class="tabmiddle"><input size=30 name="default_url" value="<?PHP echo $default_url; ?>"></td>
        <td class="tabright">Web page to show a customer when an AD has become deactive (i.e. your home page).</td>
      </tr>
      <tr>
        <td class="tableft">Logo/Icon URL (http://)</td>
        <td class="tabmiddle"><input size=30 name="logo_icon_url" value="<?PHP echo $logo_icon_url; ?>"></td>
        <td class="tabright">Logo/Icon to display in an AD (i.e. jpg, gif, png file).
							Find it on your web site, right-click, copy the URL, and paste it here.</td>
      </tr>
      <tr>
        <td class="tableft">Contact Name:</td>
        <td class="tabmiddle"><input size=30 name="contact" value="<?PHP echo $contact; ?>"></td>
        <td class="tabright"></td>
      </tr>
      <tr>
        <td class="tableft">Phone #:</td>
        <td class="tabmiddle"><input size=30 name="phone_num" value="<?PHP echo $phone_num; ?>"></td>
        <td class="tabright">Format allows only numbers, and + or - characters</td>
      </tr>
      <tr>
        <td class="tableft"></td>
        <td class="tabmiddle"><input name="btn_submit_save_profile" value="Save Profile" type="submit"></td>
        <td class="tabright"></td>
      </tr>
    </tbody>
  </table>
  </center>
</form>

<br><br>

<center>
<table cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td class="tableft"></td>
      <td class="tabmiddle"><i>Send all inquiries to: <?php echo EMAIL_ADMIN; ?></i></td>
      <td class="tabright"></td>
    </tr>
  </tbody>
</table>
</center>

</body>
</html>
