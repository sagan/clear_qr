<?php

try
{
        include "lib_app_constants.php";
}

catch (Exception $ex)
{
	//Do Nothing
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - Error</title>
</head>

<body>
<a href="/"><img alt="ClearQR" src="images/clearqr_icon.png"></a>

<hr>

<a href="/">Home</a> > <a href="login.php">Advertising Partner</a> > Error<br><br>

An unexpected error occured, please try again...

<br><br>

<i>Please notify us at <?php echo EMAIL_ADMIN; ?> if the error persists.</i>

</body>
</html>
