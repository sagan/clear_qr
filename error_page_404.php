<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - Error 404</title>
</head>

<body>
<a href="/"><img alt="ClearQR" src="images/clearqr_icon.png"></a>

<hr>

<a href="/">Home</a> > <a href="login.php">Advertising Partner</a> > Error 404<br><br>

The requested page was not found, please try again....

</body>
</html>
