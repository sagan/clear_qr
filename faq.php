<?php

try
{
        include "lib_app_constants.php";
}

catch (Exception $ex)
{
        include "lib_error_handler.php";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - FAQ</title>
</head>

<body>
<a href="/"><img alt="ClearQR" src="images/clearqr_icon.png"></a>

<hr>

<a href="/">Home</a> > FAQ<br><br>

<b>Q) How much does it cost to use ClearQR?</b>
<br>
A) Nothing, its absolutely free.
<br><br>

<b>Q) How do I use this software?</b>
<br>
A) Take a picture of a product's QR Code from your mobile phone to open its web page, and you will also receive an email with a link to its web page for later viewing.
<br><br>

<b>Q) What does a QR Code look like?</b>
<br>
A) <img alt="ClearQR" src="images/qr_test_reader.png">
<br><br>

<b>Q) Why is this better than writing it down?</b>
<br>
A) Once setup, its faster (saves paper too), and on the web page you will be able to see the full product details at your convienence.  Plus, sellers may offer you extra incentives on this web page.
<br><br>

<b>Q) Are there any other advantages to using this?</b>
<br>
A) You can use these emails as an electronic way to organize your shopping experience.  Plus, once you receive an email, you can forward it to family and friends.
<br><br>

<b>Q) Do I need special camera software for my phone?</b>
<br>
A) Yes, you need a free 'QR Reader'; if you have a smartphone there is a good chance you already have one.
<br><br>

<b>Q) How do get a free QR Reader for my mobile phone?</b>
<br>
A) Here are the detailed <a href="get_qr_reader.php">directions</a>.  Or go straight to Scanlife QR <a href="http://getscanlife.com">downloads</a>.
<br><br>

<b>Q) Ok, I have QR Reader on my phone, how can I see it in action?</b>
<br>
A) Here is a <a href="test_qr.php">test</a> page.
<br><br>

<b>Q) How do I promote my products and services with ClearQR advertising?</b>
<br>
A) Go to the <a href="login.php">Advertising Partner Login</a> page, and click 'Create New Account'.
<br><br>

<b>Q) What are some examples of where I can leverage the ClearQR technology to extend my advertising?</b>
<br>
A) You can use it for virtually anything where you have a physical presence, and want to integrate that with the Internet.  For example:
<ul>
<li>Real Estate, as an electronic for-sale listing handout.</li>
<li>Car Dealers, while people browse through car lots.</li>
<li>Coupons, surveys, and menus, in front of your restaurant.</li>
<li>Home mailers and flyers, to include with your materials.</li>
<li>Anywhere people typically use a paper handout, or have to write down something.</li>
<li>Actually, you can even put it on your web pages...</li>
</ul>

<b>Q) How much does it cost to be an Advertising Partner with ClearQR?</b>
<br>
A) It costs nothing to become an Advertising Partner, and you can generate unlimited QR Code advertisements absolutely for free.
<br><br>

<b>Q) Why is the ClearQR website so plain?</b>
<br>
A) It was designed for simplicity, and to ensure full usability over mobile devices with a small screen.
<br><br>

<i>Send all inquiries to: <?php echo EMAIL_ADMIN; ?></i>

</body>
</html>
