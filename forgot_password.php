<?PHP

try
{
session_start();
session_unset();

include "lib_app_constants.php";

$btnClickForgotPass = $_POST["btn_submit_forgot_pass"];

if ($btnClickForgotPass)
{
	include "lib_app_account.php";
	$email = trim($_POST["email"]);
	$email = strtolower($email);

	if ($email == '')
	{
		$error_tag = 'Please enter your email address';
	}
	else
	{
		//Dont even check the result, since we dont want to hint hackers if the email address was valid
		$resObj = resetPassword($email);
		header(HTTP_REDIRECT_LOCATION_DOMAIN . "/forgot_password_reset.php");
	}
}

}
catch (Exception $ex)
{
	include "lib_error_handler.php";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - Forgot Pasword</title>
</head>

<body>
<a href="/"><img alt="ClearQR" src="images/clearqr_icon.png"></a>

<hr>

<a href="/">Home</a> > <a href="login.php">Advertising Partner</a> > Forgot Password

<form method="post" action="forgot_password.php">
  <center>
  <table style="text-align: left;" cellpadding="2" cellspacing="2">
    <tbody>
      <tr>
        <td class="tableft"></td>
        <td class="tabmiddle">Enter the e-mail address associated with your account.<br>We'll email you a new password.</td>
        <td class="tabright"></td>
      </tr>
      <tr>
        <td class="tableft">Email:</td>
        <td class="tabmiddle"><input size=25 name="email" value="<?PHP echo $email; ?>">
			<input name="btn_submit_forgot_pass" value="Submit" type="submit"></td>
        <td class="tabright"></td>
      </tr>
      <tr>
        <td class="tableft"></td>
        <td class="tabmiddle"><span class="errortxt"><?PHP echo $error_tag; ?></span></td>
        <td class="tabright"></td>
      </tr>
    </tbody>
  </table>
  </center>
</form>

<br><br>

<center>
<table cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td class="tableft"></td>
      <td class="tabmiddle"><i>Send all inquiries to: <?php echo EMAIL_ADMIN; ?></i></td>
      <td class="tabright"></td>
    </tr>
  </tbody>
</table>
</center>

</body>
</html>
