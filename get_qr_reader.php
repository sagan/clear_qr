<?php

try
{
        include "lib_app_constants.php";
}

catch (Exception $ex)
{
        include "lib_error_handler.php";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - Get QR Reader</title>
</head>

<body>
<a href="/"><img alt="ClearQR" src="images/clearqr_icon.png"></a>

<hr>

<a href="/">Home</a> > Get QR Reader<br><br>

We recommend using the following:
<ul>
<li>Scanlife.com - QR <a href="http://getscanlife.com">downloads</a></li>
</ul>

If you prefer, you can find a FREE reader at your phone's app store by searching for 'QR'.<br><br>

Once you have installed a QR Reader on your mobile phone, you can use these directions to <a href="test_qr.php">test</a> it.
<br><br>

<i>Send all inquiries to: <?php echo EMAIL_ADMIN; ?></i>

</body>
</html>
