<?php

try
{
	$qrID = trim($_GET['qr']);
	$eCode = trim($_GET['ecode']);
	include "lib_app_constants.php";

	//Check if an error code is passed in (i.e. from qr_search.php)
	if ($eCode == 'QR_NONE')
	{
		$error_tag = 'The QR# was not found';
	}
	else if ($eCode == 'QR_INVALID')
	{
		$error_tag = 'The QR# must be all numeric, less than 20 digits';
	}
	else if ($eCode == 'QR_DISABLED')
	{
		$error_tag = 'The QR# has been disabled';
	}
	else if ($eCode == 'QR_INACTIVE')
	{
		$error_tag = 'The QR# is inactive';
	}
	else if ($eCode != '')
	{
		$error_tag = 'An unexpected error occured';
	}

	if ($error_tag != '')
	{
		$strEx = "index.php > QREX1-" . $error_tag . " > " . $qrID;
        error_log($strEx);
	}

}

catch (Exception $ex)
{
	include "lib_error_handler.php";
}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - Home Page</title>
</head>

<body>
<img alt="ClearQR" src="images/clearqr_icon.png">

<hr>

<a href="get_qr_reader.php">Get a QR Reader</a> for your phone.<br>
<br>

<form method="get" action="qr_search.php">
Skip the QR Reader and just type it in...<br>
QR#: <input name="qr" value="<?php echo $qrID; ?>"> <input value="Search" type="submit"><br>
<input type="hidden" id="src" name="src" value="<?php echo USER_REQUESTED_QR_MANUAL; ?>" />
<span class="errortxt"><?php echo $error_tag; ?></span>
</form>

Use ClearQR to <a href="login.php">advertise</a> your products and services.<br><br>

Questions? Read the <a href="faq.php">FAQ</a>.<br><br>

<i>Send all inquiries to: <?php echo EMAIL_ADMIN; ?></i>

</body>
</html>
