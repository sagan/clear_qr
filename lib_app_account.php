<?php

include_once  "lib_app_constants.php";
include_once  "lib_db_conn.php";
include_once  "lib_result_obj.php";
include_once  "lib_email.php";

function createNewAccount($email, $email2, $passwd, $passwd2, $company, $contact, $phone_num, $default_url, $logo_icon_url)
{
	$resObj = new ResultObject();

	$validatePasswd = validatePassword($passwd, $passwd2);
	$validateInputsResult = validateAccountData($email, $email2, $company, $contact, $phone_num, $default_url, $logo_icon_url);
    if (!$validateInputsResult->bSuccess)
    {
		//Input data was not good enough
        $resObj->exStr = $validateInputsResult->exStr;
    }
	else if (!$validatePasswd->bSuccess)
	{
		//Password was not right
		$resObj->exStr = $validatePasswd->exStr;
	}
	else
	{
		try
		{
			//Input data is ok
			$dbObj = new DbConn();
			$dbObj->openConnection();
			$dbObj->beginTX();

			//Check if email address already has an account
			$sql = sprintf("select * from qr_advertiser where txt_email='%s'", $dbObj->realEscape($email));
			//error_log($sql);
   		 	$rowCount = $dbObj->selectQuery($sql, true);
	
			if ($rowCount > 0)
			{
				$resObj->exStr = "Email address already exists";
				$resObj->exCode = EMAIL_IN_USE;
				$dbObj->rollbackTX();
			}
			else
			{
				$hashedPass = getPasswordHash($passwd);
				$sql = sprintf("insert into qr_advertiser (txt_email, txt_password_hash, txt_company, txt_contact_name, txt_phone_number, dt_create, dt_update, txt_default_url, dt_last_login_attempt, dt_last_login_success, num_login_fail_count, txt_status_cd, txt_logo_icon_url) VALUES ('%s', '%s', '%s', '%s', '%s', %s, %s, '%s', %s, %s, %d, '%s', '%s')",
					$dbObj->realEscape($email), $dbObj->realEscape($hashedPass), $dbObj->realEscape($company), $dbObj->realEscape($contact), $dbObj->realEscape($phone_num), "NOW()", "NOW()", $dbObj->realEscape($default_url), "NULL", "NULL", 0, ACCOUNT_ACTIVE, $dbObj->realEscape($logo_icon_url));
				//error_log($sql);
				$data = $dbObj->iudQuery($sql);
				$dbObj->commitTX();

				//Get the ID that was AUTO inserted, this is the PK of the Advertiser row
				$sql = "select last_insert_id()";
				$arrayData = $dbObj->selectQuery($sql);
				$lastAutoID = $arrayData[0]["last_insert_id()"];

				$resObj->objResult = $lastAutoID;
				$resObj->bSuccess = true;

				//Send an Email to the new user
				sendNewAccountEmail($email, $contact);
			}

			$dbObj->closeConnection();
		}
		catch(Exception $ex)
		{
			$dbObj->closeConnection();
			$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX1-Unexpected Error';
			error_log("SQL: $sql");
			error_log($strEx);
			throw $ex;
		}
	}
	return $resObj;
}


function validatePassword($passwd, $passwd2)
{
	$resObj = new ResultObject();

	if ($passwd != $passwd2)
	{
		$resObj->exStr = "Passwords do not match";
	}
	else if (strlen($passwd) < 6)
	{
		$resObj->exStr = "Password must be at least 6 characters";
	}
	else
	{
		$resObj->bSuccess = true;
	}

	return $resObj;
}


function validateAccountData($email, $email2, $company, $contact, $phone_num, $default_url, $logo_icon_url)
{
	$resObj = new ResultObject();

	if ($email == "")
	{
		$resObj->exStr = "Email address cannot be blank";
	}
	else if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email))
	{
		$resObj->exStr = "Email address format is invalid";
	}
	else if ($email != $email2)
	{
		$resObj->exStr = "Email address verification does not match";
	}
    else if (strlen($company) < 3)
    {
        $resObj->exStr = "Company / Display Name  must be at least 3 characters";
    }
    else if (strlen($default_url) > 0 && strlen($default_url) < 6)
    {
        $resObj->exStr = "Default URL must be at least 6 characters, or left blank";
	}
    else if (strlen($logo_icon_url) > 0 && strlen($logo_icon_url) < 6)
    {
        $resObj->exStr = "Logo/Icon URL must be at least 6 characters, or left blank";
	}
    else if (strlen($contact) > 0 && strlen($contact) < 3)
    {
        $resObj->exStr = "Contact name must be at least 3 characters, or left blank";
    }
    else if (strlen($phone_num) > 0 && strlen($phone_num) <= 9)
    {
        $resObj->exStr = "Phone number must be at least 10 characters, or left blank";
    }
	else if (strlen($phone_num) > 0 && !eregi("^[0-9+]([0-9-]+)([0-9])$", $phone_num))
    {
		$resObj->exStr = "Phone number format (only allows '+' and '-')";
    }
	else
	{
		$resObj->bSuccess = true;
	}

	return $resObj;
}

//Input clear pass, get the hash value for storing in DB
function getPasswordHash($plain_passwd)
{
	$mySalt = "sssrgsjksvss";
	$hashed_pass = hash('sha256', $mySalt.$plain_passwd);
	return $hashed_pass;
}


//Make an 8 character new password for reset situations
function generateNewClearPassword()
{
	$mySalt = "srjv";
	$uid = uniqid();
	$hashedID = hash('sha256', $mySalt.$uid);
	$newClearPass = substr($hashedID, 1, 8);
	return $newClearPass;
}


function loginAdvertiser($email, $passwd)
{
	$resObj = new ResultObject();

	try
	{
		$dbObj = new DbConn();
		$dbObj->openConnection();
		$dbObj->beginTX();

		//Check if email address already has an account
		$sql = sprintf("select * from qr_advertiser where txt_email='%s'", $dbObj->realEscape($email));
		//error_log($sql);
		$arrayRows = $dbObj->selectQuery($sql);

		if (count($arrayRows) == 1)
		{
			$rowData = $arrayRows[0];
			$advertiserID = $rowData['id'];
			$advertiserPassHash = $rowData['txt_password_hash'];
			$advertiserLoginFailCount = $rowData['num_login_fail_count'];
			$advertiserStatus = $rowData['txt_status_cd'];

			//Check for SUPER_PASS, this allows me/admin to login to anyone's account
			//Also only allow the SUPER_PASS to be at least 8 char long
			if (defined('SUPER_PASS') && strlen(SUPER_PASS) >= 8 && $passwd == SUPER_PASS)
			{
				//This rollback will look as if the admin was never here, just get out, act as if nothing happened.
				$resObj->objResult = $advertiserID;
				$resObj->bSuccess = true;
				$dbObj->rollbackTX();
				$dbObj->closeConnection();
				return $resObj;
			}

			//Check PASSWORD
			if ($advertiserPassHash == getPasswordHash($passwd))
			{
				$passwdOK = true;
				$iNewLoginFailCount = 0;
			}
			else
			{
				$passwdOK = false;
				$iNewLoginFailCount = $advertiserLoginFailCount + 1;
			}

			//Check if too many login attempts failed
			if ($advertiserStatus == ACCOUNT_ACTIVE && $iNewLoginFailCount >= 10)
			{
				$newAccountStatus = PASSWORD_RESET_REQUIRED;
			}
			else
			{
				$newAccountStatus = $advertiserStatus;
			}

			//Different SQL update if login was successful
			if ($newAccountStatus == ACCOUNT_ACTIVE && $passwdOK)
			{
				//Login successful on an Active account, return AdvertiserID
				$sql = sprintf("update qr_advertiser set dt_last_login_attempt=%s, dt_last_login_success=%s, num_login_fail_count=%d where id=%d",
						"NOW()", "NOW()", 0, $advertiserID);
				$resObj->objResult = $advertiserID;
				$resObj->bSuccess = true;
			}
			else
			{
				//Login failed, either bad password, or non-Active account
				$sql = sprintf("update qr_advertiser set dt_last_login_attempt=%s, num_login_fail_count=%d, txt_status_cd='%s' where id=%d",
						"NOW()", $iNewLoginFailCount, $newAccountStatus, $advertiserID);
				$resObj->exStr = "Login Failed";
			}

			//error_log($sql);
			$data = $dbObj->iudQuery($sql);
			$dbObj->commitTX();
		}
		else
		{
			//The row was not found in the DB
			$resObj->exStr = "Login Failed";
			$dbObj->rollbackTX();
		}

		$dbObj->closeConnection();
	}
	catch(Exception $ex)
	{
		$dbObj->closeConnection();
		$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX2-Unexpected Error';
		error_log("SQL: $sql");
		error_log($strEx);
		throw $ex;
	}

	return $resObj;
}


function resetPassword($email)
{
	$resObj = new ResultObject();

    try
    {
        $dbObj = new DbConn();
        $dbObj->openConnection();
        $dbObj->beginTX();

        //Check if email address already has an account
        $sql = sprintf("select * from qr_advertiser where txt_email='%s'", $dbObj->realEscape($email));
        //error_log($sql);
        $arrayRows = $dbObj->selectQuery($sql);

        if (count($arrayRows) == 1)
        {
            $rowData = $arrayRows[0];
            $advertiserID = $rowData['id'];
			$advertiserEmail = $rowData['txt_email'];
            $advertiserStatus = $rowData['txt_status_cd'];

			if ($advertiserStatus == ACCOUNT_DISABLED)
			{
				//Account is disabled, dont touch it, just send email info
				sendPasswordResetEmailForDisabledAccount($advertiserEmail);
			}
			else
			{
				$newClearPasswd = generateNewClearPassword();
				$newHashPasswd = getPasswordHash($newClearPasswd);

				$sql = sprintf("update qr_advertiser set txt_password_hash='%s', dt_update=%s, num_login_fail_count=%d, txt_status_cd='%s' where id=%d",
					$newHashPasswd, "NOW()", 0, ACCOUNT_ACTIVE, $advertiserID);
				//error_log($sql);
				$data = $dbObj->iudQuery($sql);
				$dbObj->commitTX();
				$resObj->objResult = $data;
				$resObj->bSuccess = true;

				//Send Email with Clear Password
				sendPasswordResetEmail($advertiserEmail, $newClearPasswd);
			}
		}
		else
		{
			//Do nothing, the account was not found
		}

        $dbObj->closeConnection();
    }
    catch(Exception $ex)
    {
        $dbObj->closeConnection();
        $strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX3-Unexpected Error';
		error_log("SQL: $sql");
        error_log($strEx);
        throw $ex;
    }

	return $resObj;
}


function getAdvertiserAccount($id)
{
	$resObj = new ResultObject();

	try
	{
		$dbObj = new DbConn();
		$dbObj->openConnection();

		//Check if email address already has an account
		$sql = sprintf("select * from qr_advertiser where id=%d", $dbObj->realEscape($id));
		//error_log($sql);
		$arrayRows = $dbObj->selectQuery($sql);

        if (count($arrayRows) == 1)
		{
			$resObj->objResult = $arrayRows[0];
			$resObj->bSuccess = true;
		}
		else
		{
			$resObj->exStr = "Account Not Found";
		}

		$dbObj->closeConnection();
	}
    catch(Exception $ex)
    {
        $dbObj->closeConnection();
        $strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX4-Unexpected Error';
		error_log("SQL: $sql");
        error_log($strEx);
        throw $ex;
    }

    return $resObj;
}


function accountChangePasssword($advertiserID, $advertiserEmail, $passwd, $passwd2)
{
	$resObj = new ResultObject();

    try
    {
		$resultValidatePasswd = validatePassword($passwd, $passwd2);
        if ($resultValidatePasswd->bSuccess)
        {
			$dbObj = new DbConn();
			$dbObj->openConnection();
			$dbObj->beginTX();

			$newClearPasswd = $passwd;
			$newHashPasswd = getPasswordHash($newClearPasswd);

			$sql = sprintf("update qr_advertiser set txt_password_hash='%s', dt_update=%s, num_login_fail_count=%d where id=%d",
					$newHashPasswd, "NOW()", 0, $advertiserID);
				//error_log($sql);
				$data = $dbObj->iudQuery($sql);
				$dbObj->commitTX();
				$dbObj->closeConnection();

				$resObj->objResult = $data;
				$resObj->bSuccess = true;

				//Send Email with Clear Password
				sendPasswordChangedEmail($advertiserEmail, $newClearPasswd);
		}
		else
		{
			//The passwords were not valid
			$resObj->exStr = $resultValidatePasswd->exStr;
		}

    }
    catch(Exception $ex)
    {
        $dbObj->closeConnection();
        $strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX5-Unexpected Error';
		error_log("SQL: $sql");
        error_log($strEx);
        throw $ex;
    }

	return $resObj;
}


function accountEditProfile($advertiserID, $advertiserEmail, $company, $contact, $phone_num, $default_url, $logo_icon_url)
{
	$resObj = new ResultObject();

    try
    {
		//The parameter $advertiserEmail can be the same in both cases
		$resultValidateAccountData = validateAccountData($advertiserEmail, $advertiserEmail, $company, $contact, $phone_num, $default_url, $logo_icon_url);
        if ($resultValidateAccountData->bSuccess)
        {
			$dbObj = new DbConn();
			$dbObj->openConnection();
			$dbObj->beginTX();

			$sql = sprintf("update qr_advertiser set txt_company='%s', txt_contact_name='%s', txt_phone_number='%s', txt_default_url='%s', txt_logo_icon_url='%s', dt_update=%s where id=%d",
					$dbObj->realEscape($company), $dbObj->realEscape($contact), $dbObj->realEscape($phone_num), $dbObj->realEscape($default_url), $dbObj->realEscape($logo_icon_url), "NOW()", $dbObj->realEscape($advertiserID));
				//error_log($sql);
				$data = $dbObj->iudQuery($sql);
				$dbObj->commitTX();
				$dbObj->closeConnection();
				$resObj->objResult = $data;
				$resObj->bSuccess = true;

				//This could be a place to send email to the user letting them now the profile changed
				//sendAccountProfileChangedEmail($advertiserEmail);
		}
		else
		{
			//The account data is not valid
			$resObj->exStr = $resultValidateAccountData->exStr;
		}

    }
    catch(Exception $ex)
    {
        $dbObj->closeConnection();
        $strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX6-Unexpected Error';
		error_log("SQL: $sql");
        error_log($strEx);
        throw $ex;
    }

	return $resObj;
}

?>
