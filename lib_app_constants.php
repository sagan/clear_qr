<?php

//Deployment Changes
define("HTTP_REDIRECT_LOCATION_DOMAIN", "Location: http://clearqr.com");
define("HTTP_REDIRECT_LOCATION_HOME", "Location: http://clearqr.com/index.php");
define("HTTP_DOMAIN", "http://clearqr.com");
define("EMAIL_ADMIN", "support@clearqr.com");

//Admin Pass
define("SUPER_PASS", "DEPLOYMENT_SECRET");

//Cookie Related (the '.' is very important to make www work
define("COOKIE_DOMAIN", ".clearqr.com");

//General Constants
define("NEWLINE", "\r\n");

//App Logic Constants
define("EMAIL_IN_USE", "EMAIL_IN_USE");

define("ACCOUNT_ACTIVE", "ACTIVE");
define("ACCOUNT_DISABLED", "DISABLED");
define("PASSWORD_RESET_REQUIRED", "PASS_RESET");

define("AD_ACTIVE", "ACTIVE");
define("AD_INACTIVE", "INACTIVE");

define("AD_COLLECT_EMAIL_OPTIONAL", "OPTIONAL");
define("AD_COLLECT_EMAIL_REQUIRE", "REQUIRE");
define("AD_COLLECT_EMAIL_DONT_ASK", "DONT_ASK");

define("USER_REQUESTED_EMAIL_TRUE", "TRUE");
define("USER_REQUESTED_EMAIL_FALSE", "FALSE");

define("USER_REQUESTED_QR_SCAN", "SCAN");
define("USER_REQUESTED_QR_MANUAL", "MANUAL");

//Session Vars
define("TERM_OF_SERVICE_VIEWED", "TERM_OF_SERVICE_VIEWED");
define("ADVERTISER_ID", "ADVERTISER_ID");

?>
