<?php

include_once "lib_app_constants.php";
include_once "lib_db_conn.php";
include_once "lib_result_obj.php";
include_once "lib_email.php";


function createNewQrAd($advertiserID, $redirect_url, $internal_id, $internal_desc, $public_desc, $public_detail, $collect_email)
{
	$resObj = new ResultObject();
	$status_cd = AD_ACTIVE; //Default the Ad to being ACTIVE
	$validateInputResults = validateAd($redirect_url, $internal_id, $internal_desc, $public_desc, $public_detail, $collect_email, $status_cd);

	if (!$validateInputResults->bSuccess)
	{
		$resObj->exStr = $validateInputResults->exStr;
	}
	else
	{
		try
		{
			$dbObj = new DbConn();
			$dbObj->openConnection();
			$dbObj->beginTX();

			//Check if you already have this internal_id in your ads
			$sql = sprintf("select * from qr_ad where fk_advertiser_id=%d and txt_internal_id='%s'",
							$dbObj->realEscape($advertiserID), $dbObj->realEscape($internal_id));
			//error_log($sql);
			$arrayRows = $dbObj->selectQuery($sql);

			if (count($arrayRows) != 0)
			{
				$resObj->exStr = "You already have an AD with this Internal ID";
				$dbObj->rollbackTX();
			}
			else
			{
				$sql = sprintf("insert into qr_ad (fk_advertiser_id, txt_url_page, dt_create, dt_update, txt_status_cd, txt_internal_id, txt_internal_desc, txt_public_desc, txt_public_detail, txt_collect_user_email) VALUES (%d, '%s', %s, %s, '%s', '%s', '%s', '%s', '%s', '%s')",
				$dbObj->realEscape($advertiserID), $dbObj->realEscape($redirect_url), "NOW()", "NOW()", $dbObj->realEscape($status_cd), $dbObj->realEscape($internal_id), $dbObj->realEscape($internal_desc), $dbObj->realEscape($public_desc), $dbObj->realEscape($public_detail), $dbObj->realEscape($collect_email));
				//error_log($sql);
				$data = $dbObj->iudQuery($sql);
				$dbObj->commitTX();

				$resObj->bSuccess = true;
			}

			$dbObj->closeConnection();
		}
		catch(Exception $ex)
		{
			$dbObj->closeConnection();
			$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX1-Unexpected Error';
			error_log("SQL: $sql");
			error_log($strEx);
	        throw $ex;
		}

	}
    return $resObj;
}


function validateAd($redirect_url, $internal_id, $internal_desc, $public_desc, $public_detail, $collect_email, $status_cd)
{
	//PublicDetail can be blank
	$resObj = new ResultObject();

	if (strlen($redirect_url) <= 5)
	{
		$resObj->exStr = "Redirect URL must be at least 6 characters";
	}
	else if (strlen($internal_id) <= 0)
	{
		$resObj->exStr = "Internal ID cannot be blank";
	}
	else if (strlen($internal_desc) <= 0)
	{
		$resObj->exStr = "Internal Description cannot be blank";
	}
	else if (strlen($public_desc) <= 0)
	{
		$resObj->exStr = "Public Description cannot be blank";
	}
	else if (!($collect_email == AD_COLLECT_EMAIL_OPTIONAL || $collect_email == AD_COLLECT_EMAIL_REQUIRE || $collect_email == AD_COLLECT_EMAIL_DONT_ASK))
	{
		$resObj->exStr = "Collect User Email has an unexpected value";
	}
	else if (!($status_cd == AD_ACTIVE || $status_cd == AD_INACTIVE))
	{
		$resObj->exStr = "Status has an unexpected value";
	}
	else
	{
		$resObj->bSuccess = true;
	}

	return $resObj;
}


function getAds($advertiserID, $sortOrder=NULL)
{
	$resObj = new ResultObject();
	try
	{
		$dbObj = new DbConn();
		$dbObj->openConnection();

		//Set the sort order
		if ($sortOrder=='iid')
		{
			$sortOrderColumn = 'txt_internal_id';
		}
		else if ($sortOrder=='dtup')
		{
			$sortOrderColumn = 'dt_update desc';
		}
		else if ($sortOrder=='stat')
		{
			$sortOrderColumn = 'txt_status_cd';
		}
		else
		{
			$sortOrderColumn = 'id';
		}

		$sql = sprintf("select * from qr_ad where fk_advertiser_id='%s' order by %s",
				$dbObj->realEscape($advertiserID), $dbObj->realEscape($sortOrderColumn));
		//error_log($sql);
		$arrayRows = $dbObj->selectQuery($sql);

		$resObj->objResult = $arrayRows;
		$resObj->bSuccess = true;

		$dbObj->closeConnection();
	}
	catch(Exception $ex)
	{
		$dbObj->closeConnection();
		$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX2-Unexpected Error';
		error_log("SQL: $sql");
		error_log($strEx);
        throw $ex;
	}

    return $resObj;
}


//The parameter of $advertiserID is for security purposes, since its a session var, and cant be client manipulated
function getAd($qrID, $advertiserID)
{
	$resObj = new ResultObject();
	try
	{
		$dbObj = new DbConn();
		$dbObj->openConnection();

		$sql = sprintf("select * from qr_ad where id=%d and fk_advertiser_id=%d",
				$dbObj->realEscape($qrID), $dbObj->realEscape($advertiserID));
		//error_log($sql);
		$arrayRows = $dbObj->selectQuery($sql);

		if (count($arrayRows) == 1)
		{
			$resObj->objResult = $arrayRows[0];
			$resObj->bSuccess = true;
		}
		else
		{
			$resObj->exStr = "QR ID not found";
		}

		$dbObj->closeConnection();
	}
	catch(Exception $ex)
	{
		$dbObj->closeConnection();
		$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX3-Unexpected Error';
		error_log("SQL: $sql");
		error_log($strEx);
        throw $ex;
	}

    return $resObj;
}


//The parameter of $advertiserID is for security purposes, since its a session var, and cant be client manipulated
function editExistingQrAd($qrID, $advertiserID, $redirect_url, $internal_id, $internal_desc, $public_desc, $public_detail, $collect_email, $status_cd)
{
	$resObj = new ResultObject();
	$validateInputResults = validateAd($redirect_url, $internal_id, $internal_desc, $public_desc, $public_detail, $collect_email, $status_cd);

	if (!$validateInputResults->bSuccess)
	{
		$resObj->exStr = $validateInputResults->exStr;
	}
	else
	{
		try
		{
			$dbObj = new DbConn();
			$dbObj->openConnection();
			$dbObj->beginTX();

			//Check if you already have this internal_id in your ads
			//Dont include row you are editing in your resultset, its to check for other rows
			$sql = sprintf("select * from qr_ad where fk_advertiser_id=%d and txt_internal_id='%s' and not id=%d",
							$dbObj->realEscape($advertiserID), $dbObj->realEscape($internal_id), $dbObj->realEscape($qrID));
			//error_log($sql);
			$arrayRows = $dbObj->selectQuery($sql);

			if (count($arrayRows) != 0)
			{
				$resObj->exStr = "You already have an AD with this Internal ID";
				$dbObj->rollbackTX();
			}
			else
			{
				$sql = sprintf("update qr_ad set txt_url_page='%s', dt_update=%s, txt_status_cd='%s', txt_internal_id='%s', txt_internal_desc='%s', txt_public_desc='%s', txt_public_detail='%s', txt_collect_user_email='%s' where id=%d and fk_advertiser_id=%d",
					$dbObj->realEscape($redirect_url), "NOW()", $dbObj->realEscape($status_cd), $dbObj->realEscape($internal_id), $dbObj->realEscape($internal_desc), $dbObj->realEscape($public_desc), $dbObj->realEscape($public_detail), $dbObj->realEscape($collect_email), $dbObj->realEscape($qrID), $dbObj->realEscape($advertiserID));
				//error_log($sql);
				$data = $dbObj->iudQuery($sql);
				$dbObj->commitTX();

				$resObj->bSuccess = true;
			}

			$dbObj->closeConnection();
		}
		catch(Exception $ex)
		{
			$dbObj->closeConnection();
			$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX4-Unexpected Error';
			error_log("SQL: $sql");
			error_log($strEx);
	        throw $ex;
		}

	}
    return $resObj;
}


?>
