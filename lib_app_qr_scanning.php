<?php

include_once "lib_app_constants.php";
include_once "lib_db_conn.php";
include_once "lib_result_obj.php";
include_once "lib_email.php";


//Call when generating a QR, to get the Redirect URL
function getAdForQrScan($qrID)
{
	$resObj = new ResultObject();
	try
	{
		$dbObj = new DbConn();
		$dbObj->openConnection();

		$sql = sprintf("select * from qr_ad where id=%d", $dbObj->realEscape($qrID));
		//error_log($sql);
		$arrayRows = $dbObj->selectQuery($sql);

		if (count($arrayRows) == 1)
		{
			$resObj->objResult = $arrayRows[0];
			$resObj->bSuccess = true;
		}
		else
		{
			$resObj->exStr = "QR ID not found";
		}

		$dbObj->closeConnection();
	}
	catch(Exception $ex)
	{
		$dbObj->closeConnection();
		$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX1-Unexpected Error';
		error_log("SQL: $sql");
		error_log($strEx);
        throw $ex;
	}

    return $resObj;
}

function validateEmail($email)
{
	$resObj = new ResultObject();

	if ($email == "")
	{
		$resObj->exStr = "Email address cannot be blank";
	}
	else if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email))
	{
		$resObj->exStr = "Email address format is invalid";
	}
	else
	{
		$resObj->bSuccess = true;
	}

	return $resObj;
}


function getUser($cuid)
{
    $resObj = new ResultObject();

	try
	{
		$dbObj = new DbConn();
		$dbObj->openConnection();

		if ($cuid != '')
		{
			$sql = sprintf("select * from qr_user where id=%d", $dbObj->realEscape($cuid));
			//error_log($sql);
			$arrayRows = $dbObj->selectQuery($sql);

			if (count($arrayRows) != 0)
			{
				$resObj->objResult = $arrayRows[0];
				$resObj->bSuccess = TRUE;
        	}
		}

        $dbObj->closeConnection();
	}
	catch(Exception $ex)
	{
		$dbObj->closeConnection();
		$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX3-Unexpected Error';
		error_log("SQL: $sql");
		error_log($strEx);
		throw $ex;
	}

    return $resObj;
}


function getUserTrackByEmail($email, $cuid)
{
    $resObj = new ResultObject();
	$resArray = array();

	try
	{
		$dbObj = new DbConn();
		$dbObj->openConnection();
		$dbObj->beginTX();

		$email = strtolower($email);
		if ($email != '')
		{
			//Check if you already have this email as a user
			$sql = sprintf("select * from qr_user where txt_email='%s'", $dbObj->realEscape($email));
			//error_log($sql);
			$arrayRows = $dbObj->selectQuery($sql);

			if (count($arrayRows) != 0)
			{
				//The Email address exists, update its timestamp, and use it
				$sql = sprintf("update qr_user set dt_last_request=%s where txt_email='%s'",
						"NOW()", $dbObj->realEscape($email));
				$data = $dbObj->iudQuery($sql);
				$resArray['cuid'] = $arrayRows[0]['id'];
				$resArray['email'] = $email;
			}
			else if ($cuid != '')
			{
				//The email was not found, but wait, a cuid was passed it, see if it exists with a txt_email=NULL
				//We only look for txt_email=NULL, because we never want to re-write a complete user-row
				$sql = sprintf("select * from qr_user where id=%d and txt_email is NULL", $dbObj->realEscape($cuid));
				$arrayRows = $dbObj->selectQuery($sql);

				if (count($arrayRows) != 0)
				{
					//Only way to not find email, but have a cuid, is where a user was not submitting email=yes before
					//The cuid was found, update it with email and timestamp, lets use it
					$sql = sprintf("update qr_user set txt_email='%s', dt_last_request=%s where id=%d and txt_email is NULL",
							$dbObj->realEscape($email), "NOW()", $dbObj->realEscape($cuid));
					$data = $dbObj->iudQuery($sql);
					$resArray['cuid'] = $cuid;
					$resArray['email'] = $email;
				}
				else
				{
					//The cuid was not found, lets make a new row, and throw out the cuid that was passed in (i.e. hacker)
					$sql = sprintf("insert into qr_user (txt_email, dt_create, dt_last_request) VALUES ('%s', %s, %s)",
						$dbObj->realEscape($email), "NOW()", "NOW()");
					$data = $dbObj->iudQuery($sql);
					$resArray['cuid'] = $dbObj->getLastAutoID();
					$resArray['email'] = $email;
				}
				
			}
			else
			{
				//The email was not found, and the cuid was blank; First time for this user
				$sql = sprintf("insert into qr_user (txt_email, dt_create, dt_last_request) VALUES ('%s', %s, %s)",
					$dbObj->realEscape($email), "NOW()", "NOW()");
				$data = $dbObj->iudQuery($sql);
				$resArray['cuid'] = $dbObj->getLastAutoID();
				$resArray['email'] = $email;
			}
		}
		else
		{
			//No email, cant do much with that; shouldnt be calling this function anyway
			//Could throw an Exception, but I dont want to stop the user
			error_log("QREX4b-Function lib_app_qr_scanning.php > getUserTrackByEmail() called with no email address");
			$resArray['cuid'] = '';
			$resArray['email'] = '';
		}

		$dbObj->commitTX();
		$dbObj->closeConnection();
	}
	catch(Exception $ex)
	{
		$dbObj->closeConnection();
		$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX4-Unexpected Error';
		error_log("SQL: $sql");
		error_log($strEx);
		throw $ex;
	}

	$resObj->objResult = $resArray;
	return $resObj;
}


function getUserTrackByCUID($cuid)
{
    $resObj = new ResultObject();
	$resArray = array();

	try
	{
		$dbObj = new DbConn();
		$dbObj->openConnection();
		$dbObj->beginTX();

		if ($cuid != '')
		{
			$sql = sprintf("select * from qr_user where id=%d", $dbObj->realEscape($cuid));
			//error_log($sql);
			$arrayRows = $dbObj->selectQuery($sql);

			if (count($arrayRows) != 0)
			{
				//The CUID is found, update its timestamp, happens when a user does email=no
				$sql = sprintf("update qr_user set dt_last_request=%s where id=%d",
						"NOW()", $dbObj->realEscape($cuid));
				//error_log($sql);
				$data = $dbObj->iudQuery($sql);
				$resArray['cuid'] = $cuid;
				$resArray['email'] = $arrayRows[0]['txt_email'];
	       	}
			else
			{
				//The CUID was not found, replace it with a new one, shouldnt happen but hackers exist
				$sql = sprintf("insert into qr_user (txt_email, dt_create, dt_last_request) VALUES (%s, %s, %s)",
					"NULL", "NOW()", "NOW()");
				//error_log($sql);
				$data = $dbObj->iudQuery($sql);
				$resArray['cuid'] = $dbObj->getLastAutoID();
				$resArray['email'] = '';
			}
		}
		else
		{
			//First time for this user, make a new one, the email=NULL it will get updated during an email=yes later
			$sql = sprintf("insert into qr_user (txt_email, dt_create, dt_last_request) VALUES (%s, %s, %s)",
				"NULL", "NOW()", "NOW()");
			$data = $dbObj->iudQuery($sql);
			$resArray['cuid'] = $dbObj->getLastAutoID();
			$resArray['email'] = '';
		}

		$dbObj->commitTX();
		$dbObj->closeConnection();
	}
	catch(Exception $ex)
	{
		$dbObj->closeConnection();
		$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX5-Unexpected Error';
		error_log("SQL: $sql");
		error_log($strEx);
		throw $ex;
	}

	$resObj->objResult = $resArray;
	return $resObj;
}


?>
