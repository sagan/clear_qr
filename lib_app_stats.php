<?php

include_once "lib_app_constants.php";
include_once "lib_db_conn.php";
include_once "lib_result_obj.php";


//User to track scans
function newViewRequest($advertiserID, $adID, $txtStatus, $txtURL, $txtInternalID, $cuid, $txtUserAgent, $txtClientIP, $txtSource, $txtEmailType, $txtEmailResult)
{
    $resObj = new ResultObject();

	try
	{
		$dbObj = new DbConn();
		$dbObj->openConnection();
		$dbObj->beginTX();

		$sql = sprintf("insert into qr_view_request (fk_advertiser_id, fk_ad_id, txt_ad_status_cd, txt_ad_url_page, txt_ad_internal_id, fk_user_id, txt_client_user_agent, txt_client_ip, dt_create, txt_source_cd, txt_collect_user_email, txt_user_requested_email) VALUES (%d, %d, '%s', '%s', '%s', %d, '%s', '%s', %s, '%s', '%s', '%s')",
				$dbObj->realEscape($advertiserID), $dbObj->realEscape($adID), $dbObj->realEscape($txtStatus), $dbObj->realEscape($txtURL), $dbObj->realEscape($txtInternalID), $dbObj->realEscape($cuid), $dbObj->realEscape($txtUserAgent), $dbObj->realEscape($txtClientIP), "NOW()", $dbObj->realEscape($txtSource), $dbObj->realEscape($txtEmailType), $dbObj->realEscape($txtEmailResult));
		//error_log($sql);
		$data = $dbObj->iudQuery($sql);
		$resObj->objResult = $data;

		$dbObj->commitTX();
		$dbObj->closeConnection();
	}
	catch(Exception $ex)
	{
		$dbObj->closeConnection();
		$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX1-Unexpected Error';
		error_log("SQL: $sql");
		error_log($strEx);
		//Dont throw an Exception, this should never stop the user
		//throw $ex;
	}

	return $resObj;
}


//The parameter of $advertiserID is for security purposes, since its a session var, and cant be client manipulated
function getCountsByAd($advertiserID)
{
    $resObj = new ResultObject();
    try
    {
        $dbObj = new DbConn();
        $dbObj->openConnection();

        $sql = sprintf("select fk_ad_id qr_id, count(fk_ad_id) num_view_count, max(dt_create) dt_view_last from qr_view_request where fk_advertiser_id=%d group by fk_ad_id", $dbObj->realEscape($advertiserID));
        //error_log($sql);
        $arrayRows = $dbObj->selectQuery($sql);

		$resObj->objResult = convertQrRowDataToHashtable($arrayRows);
        $resObj->bSuccess = true;
        $dbObj->closeConnection();
    }
    catch(Exception $ex)
    {
        $dbObj->closeConnection();
        $strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX2-Unexpected Error';
        error_log("SQL: $sql");
        error_log($strEx);
        throw $ex;
    }

    return $resObj;
}


//Converts rows of data summarized on QR_ID to a hashtable for fast lookups in application logic
function convertQrRowDataToHashtable($adCountRows)
{
	$htRows = array();

	foreach ($adCountRows as &$row)
	{
		//Get the QR_ID, and make a hashtable with that beng the key
		$id = $row['qr_id'];
		$htRows[$id] = $row;
		
	}

	return $htRows;
}


?>
