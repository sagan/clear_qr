<?php

include_once "lib_app_constants.php";
include_once "lib_db_conn.php";
include_once "lib_result_obj.php";


function getTestRows()
{
	$resObj = new ResultObject();

	try
	{
		$dbObj = new DbConn();
		$dbObj->openConnection();
		$data = $dbObj->selectQuery("select * from qr_tester");
		$dbObj->closeConnection();
		$resObj->objResult = $data;
		$resObj->bSuccess = true;
	}
	catch(Exception $ex)
	{
		$dbObj->closeConnection();
		$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX1-Unexpected Error';
		error_log($strEx);
		throw $ex;
	}
	return $resObj;
}

function insertTestRow()
{
	$resObj = new ResultObject();

	try
	{
		$dbObj = new DbConn();
		$dbObj->openConnection();
		$dbObj->beginTX();
		$rowCount = $dbObj->selectQuery("select * from qr_tester", true);
		$sql = sprintf("insert into qr_tester (str_a, num_b, dt_c) VALUES ('%s', %d, %s)",
					$dbObj->realEscape("' OR ''='"), $dbObj->realEscape($rowCount), "NOW()");
		//$sql = sprintf("insert into qr_tester (str_a, num_b, dt_c) VALUES ('%s', %d, '%s')",
		//				$dbObj->realEscape("' OR ''='"), $dbObj->realEscape($rowCount), "2008-7-4 3:4:5");
		error_log($sql);
		$data = $dbObj->iudQuery($sql);
		$dbObj->commitTX();
		$dbObj->closeConnection();
		$resObj->objResult = $data;
		$resObj->bSuccess = true;
	}
	catch(Exception $ex)
	{
		//$dbObj->rollbackTX();
		$dbObj->closeConnection();
		$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX2-Unexpected Error';
		error_log($strEx);
		throw $ex;
	}

	return $resObj;
}


?>
