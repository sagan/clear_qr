<?php

//http://docs.php.net/manual/en/language.oop5.visibility.php
//http://www.php.net/manual/en/ref.mysql.php

class DbConn
{
	//set up the class
	private $dbhost = "localhost";
	private $db = "clearqrc_prod";
	private $dbuser = "clearqrc_write";
	private $dbpassword = "DEPLOYMENT_SECRET";
	private $dbconnection = false;


//Closesthe DB connection, or silently logs an error
function closeConnection()
{
	try
	{
		if($this->dbconnection)
		{
			mysql_close($this->dbconnection);
			$this->dbconnection = false;
		}
	}
	catch (Exception $ex)
	{
		$this->dbconnection = false;
		error_log($ex);
		$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX6-Cant close DB connection';
		error_log($strEx);
	}
}


//Use to avois SQL injection attacks
function realEscape($string)
{
	return mysql_real_escape_string($string,$this->dbconnection);
}

function beginTX()
{
	if($this->dbconnection)
		mysql_query("START TRANSACTION", $this->dbconnection);
}

function commitTX()
{
	if($this->dbconnection)
		mysql_query("COMMIT", $this->dbconnection);
}

function rollbackTX()
{
	if($this->dbconnection)
		mysql_query("ROLLBACK", $this->dbconnection);
}

function getLastAutoID()
{
	if($this->dbconnection)
	{
		$lastID = mysql_insert_id($this->dbconnection);
	}

	if ($lastID > 0)
		return $lastID;
	else
		throw new Exception("No AutoIncrement ID found");
}


//Simple connects to the DB, or throws an Exception
function openConnection()
{
	$this->closeConnection();

	$this->dbconnection = mysql_connect($this->dbhost, $this->dbuser, $this->dbpassword);
	if ($this->dbconnection)
	{
		$dbready = mysql_select_db($this->db, $this->dbconnection);
		if ($dbready)
		{
			return true;
		}
		else
		{
			$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX1-Cant connect to DB';
			error_log($strEx);
			$this->closeConnection();
			throw new Exception($strEx);
		}
	}
	else
	{
		$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX2-Cant connect to DB';
		error_log($strEx);
		$this->closeConnection();
		throw new Exception($strEx);
	}
}


//Returns # of rows affected by SQL, or an Array of hashRows, or throws an Exception
function selectQuery($sql, $bGetRowCountOnly=false)
{
	$resultset = array();

	if (!$this->dbconnection)
	{
		$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX3-DB Connection is closed';
		error_log($strEx);
		$this->closeConnection();
		throw new Exception($strEx);
	}

	//Do the actual query
	$result = mysql_query($sql, $this->dbconnection);

	if($result && !$bGetRowCountOnly)
	{
		//Put each row of hashData into an array
		while ($row = mysql_fetch_assoc($result))
		{
			$resultset[] = $row;	
		}
	}
	else if ($result && $bGetRowCountOnly)
	{
		$resultset = mysql_num_rows($result);
	}
	else
	{
		$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX7-DB query failed, no result';
		error_log($strEx);
		$this->closeConnection();
		throw new Exception($strEx);
	}

	mysql_free_result($result);
	return $resultset;
}


//Returns # of rows affected by SQL, or throws an Exception
function iudQuery($sql)
{
	$rowCount = -1;

	if (!$this->dbconnection)
	{
		$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX4-DB Connection is closed';
		error_log($strEx);
		$this->closeConnection();
		throw new Exception($strEx);
	}

	//Do the actual query
	$result = mysql_query($sql, $this->dbconnection);

	if($result)
	{
		$rowCount = mysql_affected_rows($this->dbconnection);
	}
	else
	{
		$strEx = __CLASS__." > ".__FUNCTION__." > ".'QREX5-DB query failed, no result';
		error_log($strEx);
		$this->closeConnection();
		throw new Exception($strEx);
	}

	return $rowCount;
}


} //end of class

?>
