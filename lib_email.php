<?php

include "lib_app_constants.php";
//http://php.net/manual/en/function.mail.php

function sendEmail($to, $subject, $message, $from=NULL)
{
	if ($from == "NO_REPLY")
	{
		$headers = 'From: "ClearQR" <support@clearqr.com>' . NEWLINE .
		'Reply-To: support@clearqr.com' . NEWLINE .
		'X-Mailer: PHP/' . phpversion();
	}
	else
	{
		$headers = 'From: "ClearQR" <support@clearqr.com>' . NEWLINE .
		'Reply-To: support@clearqr.com' . NEWLINE .
		'X-Mailer: PHP/' . phpversion();
	}

	$resultMail = mail($to, $subject, $message, $headers, '-f<support@clearqr.com>');
	//$resultMail = mail($to, $subject, $message, $headers);
	//$resultMail = mail($to, $subject, $message);

	if (!$resultMail)
	{
		error_log("QREX Mail Fail: $to - $subject");
	}
	else
	{
		//error_log("Mail Sent: $to - $subject");
	}
}


function sendNewAccountEmail($to, $contactName)
{
	$subject = "Welcome to ClearQR";

	$message = "Hi " . $contactName . "," . NEWLINE . NEWLINE .

	"Thank you for signing up with ClearQR, the next generation mobile barcode technology." . NEWLINE . NEWLINE .

	"Here is your login info:" . NEWLINE .
	"Username = " . $to . NEWLINE .
	"To retrieve your password visit " . HTTP_DOMAIN . "/forgot_password.php" . NEWLINE . NEWLINE .

	"Do not share your username and password with anybody." . NEWLINE .
	"Login - " . HTTP_DOMAIN . "/login.php" . NEWLINE .
	"FAQ - " . HTTP_DOMAIN . "/faq.php" . NEWLINE . NEWLINE .

	"Please contact us at roamify@gmail.com for any further questions." . NEWLINE .
	"Welcome to ClearQR..." . NEWLINE;

	sendEmail($to, $subject, $message);
}


function sendPasswordResetEmail($to, $newPasswd)
{
	$subject = "Password Reset - ClearQR";

	$message = "Your ClearQR password has been reset." . NEWLINE . NEWLINE .

	"Here is your login info:" . NEWLINE .
	"Username = " . $to . NEWLINE .
	"Password = " . $newPasswd . NEWLINE . NEWLINE .

	"Do not share your username and password with anybody." . NEWLINE .
	"Login - " . HTTP_DOMAIN . "/login.php" . NEWLINE .
	"FAQ - " . HTTP_DOMAIN . "/faq.php" . NEWLINE . NEWLINE .

	"Please contact us at roamify@gmail.com for any further questions." . NEWLINE;

	sendEmail($to, $subject, $message);
}


function sendPasswordResetEmailForDisabledAccount($to)
{
	$subject = "Password Reset - ClearQR";

	$message = "Your ClearQR password cannot be reset, due to your account has been disabled." . NEWLINE . NEWLINE .

	"Here is your login info:" . NEWLINE .
	"Username = " . $to . NEWLINE .
	"Please contact us at roamify@gmail.com to re-active your account." . NEWLINE . NEWLINE .

	"Do not share your username and password with anybody." . NEWLINE .
	"Login - " . HTTP_DOMAIN . "/login.php" . NEWLINE .
	"FAQ - " . HTTP_DOMAIN . "/faq.php" . NEWLINE . NEWLINE .

	"Please contact us at roamify@gmail.com for any further questions." . NEWLINE;

	sendEmail($to, $subject, $message);
}


function sendPasswordChangedEmail($to, $newPasswd)
{
	$subject = "Password Changed - ClearQR";

	$message = "Your ClearQR password has been changed." . NEWLINE . NEWLINE .

	"Here is your login info:" . NEWLINE .
	"Username = " . $to . NEWLINE .
	"To retrieve your password visit " . HTTP_DOMAIN . "/forgot_password.php" . NEWLINE . NEWLINE .

	"Do not share your username and password with anybody." . NEWLINE .
	"Login - " . HTTP_DOMAIN . "/login.php" . NEWLINE .
	"FAQ - " . HTTP_DOMAIN . "/faq.php" . NEWLINE . NEWLINE .

	"Please contact us at roamify@gmail.com for any further questions." . NEWLINE;

	sendEmail($to, $subject, $message);
}

function sendEmailAdTouser($qr_id, $to, $company, $pub_desc, $pub_detail, $link)
{
	$subject = "$company sent you a link (QR#$qr_id)";

	$message = "You requested QR#$qr_id from $company:" . NEWLINE . NEWLINE .

	"Description: $pub_desc" . NEWLINE . NEWLINE .

	"Details: $pub_detail" . NEWLINE . NEWLINE .

	"http://$link" . NEWLINE . NEWLINE .

	"Powered by " . HTTP_DOMAIN . NEWLINE;

	sendEmail($to, $subject, $message, "NO_REPLY");
}

?>
