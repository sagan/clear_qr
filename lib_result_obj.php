<?php


//This class is for encapsulating the result
//Where th result can be what you expect (i.e. a row from a DB table)
//Or it can be an error, like no row found

class ResultObject
{
	//set up the class
	public $objResult = NULL;
	public $bSuccess = false;
	public $exStr = NULL;
	public $exCode = NULL;
}

?>
