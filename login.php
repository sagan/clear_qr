<?PHP

try
{
session_start();

include "lib_app_constants.php";

$btnClickLogin = $_POST["btn_submit_login"];
$advertiserID = $_SESSION[ADVERTISER_ID];

//Check if they are already logged in.
if ($advertiserID)
{
	//header("Location: http://clearqr.com/account_summary.php");
	header(HTTP_REDIRECT_LOCATION_DOMAIN . "/account_summary.php");
	exit();
}

if ($btnClickLogin)
{
	include "lib_app_account.php";
	$email = trim($_POST["email"]);
	$passwd = $_POST["passwd"];
	$email = strtolower($email);

	$resObj = loginAdvertiser($email, $passwd);
	if ($resObj->bSuccess)
	{
		$_SESSION[ADVERTISER_ID] = $resObj->objResult;
		//header("Location: http://clearqr.com/account_summary.php");
		header(HTTP_REDIRECT_LOCATION_DOMAIN . "/account_summary.php");
		exit();
	}
	else
	{
		$error_tag = $resObj->exStr;
	}
}

}
catch (Exception $ex)
{
	include "lib_error_handler.php";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - Login</title>
</head>

<body>
<a href="/"><img alt="ClearQR" src="images/clearqr_icon.png"></a>

<hr>

<a href="/">Home</a> > Advertising Partner > Login

<form method="post" action="login.php">
  <center>
  <table style="text-align: left;" cellpadding="2" cellspacing="2">
    <tbody>
      <tr>
        <td class="tableft"></td>
        <td class="tabmiddle"><span class="errortxt"><?PHP echo $error_tag; ?></span></td>
        <td class="tabright"></td>
      </tr>
      <tr>
        <td class="tableft">Email:</td>
        <td class="tabmiddle"><input size=25 name="email" value="<?PHP echo $email; ?>"></td>
        <td class="tabright"></td>
      </tr>
      <tr>
        <td class="tableft">Password:</td>
        <td class="tabmiddle"><input size=25 name="passwd" type="password">
	<input name="btn_submit_login" value="Submit" type="submit"></td>
        <td class="tabright"></td>
      </tr>
      <tr>
        <td class="tableft"></td>
        <td class="tabmiddle"><a href="forgot_password.php">Forgot Password</a></td>
        <td class="tabright"></td>
      </tr>
      <tr>
        <td class="tableft"></td>
        <td class="tabmiddle"><a href="create_account_agreement.php">Create New Account</a></td>
        <td class="tabright"></td>
      </tr>
    </tbody>
  </table>
  </center>
</form>

<br>

<b>How to Create, Manage, and Track your ads with ClearQR:</b><br>
Step 1: Signup for a free partner account.<br>
Step 2: Create your ads. Examples: Real Estate, Car Sales, To-Go Menus, Coupons, Surveys, to name a few.<br>
Step 3: Print them out, and place them in strategic locations.<br>
Step 4: Dynamically manage/change ads, as needed.<br>
Step 5: Track the # of people that view your ads.<br>
<br>
Questions? Read the <a href="faq.php">FAQ</a><br>

<br>

<center>
<table cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td class="tableft"></td>
      <td class="tabmiddle"><i>Send all inquiries to: <?php echo EMAIL_ADMIN; ?></i></td>
      <td class="tabright"></td>
    </tr>
  </tbody>
</table>
</center>

</body>
</html>
