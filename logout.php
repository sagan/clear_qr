<?PHP
session_start();
session_unset();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - Logout</title>
</head>

<body>
<a href="/"><img alt="ClearQR" src="images/clearqr_icon.png"></a>

<hr>

<a href="/">Home</a> > <a href="login.php">Advertising Partner</a> > Logout<br><br>

You have been logged out.  Here is a link to <a href="login.php">login</a> again.

</body>
</html>
