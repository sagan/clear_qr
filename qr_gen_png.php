<?php

try
{
	include "phpqrcode/qrlib.php";
	include "lib_app_constants.php";

	$qrID = trim($_GET["qr"]);

	//Anything more than 20,something must be wrong (i.e. hackers)
	if (strlen($qrID) > 20)
	{
		//header("Location: http://clearqr.com/images/invalid_qr.png");
		header(HTTP_REDIRECT_LOCATION_DOMAIN . "/images/invalid_qr.png");
	    exit();
	}
	else
	{
		//http://clearqr.com/qr_search.php?qr=123
		$qrURL = HTTP_DOMAIN . "/qr_search.php?qr=" . $qrID;
	}

	QRcode::png($qrURL);
	exit();

}

catch (Exception $ex)
{
	//header("Location: http://clearqr.com/images/invalid_qr.png");
	header(HTTP_REDIRECT_LOCATION_DOMAIN . "/images/invalid_qr.png");
	exit();
}

?>

