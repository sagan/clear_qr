<?php

try
{
	include "phpqrcode/qrlib.php";
	include "lib_app_constants.php";
	include "lib_app_account.php";
	include "lib_app_qr_scanning.php";
	include "lib_app_stats.php";

	$qrID = trim($_REQUEST["qr"]);
	$email = trim($_REQUEST["email"]);
	$srcQR = trim($_REQUEST["src"]);
	$btnNoEmail = $_POST["btn_no_email"];
	$btnYesEmail = $_POST["btn_yes_email"];
	$cuid = $_COOKIE["cuid"];

	//People are inconsistent, so we must accommodate
	$email = strtolower($email);

	//Input Validation: Check its a number that less than 20 digits
	if (strlen($qrID) > 20 || is_numeric($qrID) != TRUE)
	{
		$redirectURL = HTTP_REDIRECT_LOCATION_HOME . '?qr=' . $qrID . '&ecode=QR_INVALID';
		header($redirectURL);
		exit();
	}

	//Fetch the Ad from the DB
	$adResultObj = getAdForQrScan($qrID);
	if (!$adResultObj->bSuccess)
	{
		//The AD (qrID) was NOT found
		$redirectURL = HTTP_REDIRECT_LOCATION_HOME . '?qr=' . $qrID . '&ecode=QR_NONE';
		header($redirectURL);
	    exit();
	}
	else
	{
		//The AD was found, get the Advertiser object
		$adDataRow = $adResultObj->objResult;
		$advertiserID = $adDataRow['fk_advertiser_id'];
		$advertiserResultObj = getAdvertiserAccount($advertiserID);

		//Make sure you have the Advertiser object
		if (!$advertiserResultObj->bSuccess)
		{
			//Something wasnt right
			$strEx = 'Ad for qrID (' . $qrID . ') found, but advertiser row was not';
			throw new Exception($strEx);
		}
		else
		{
			//Got the Advertiser object
			$advertiserDataRow = $advertiserResultObj->objResult;
		}

		//If the Advertiser's account has been disabled, show an error
		if ($advertiserDataRow['txt_status_cd'] == ACCOUNT_DISABLED)
		{
			$redirectURL = HTTP_REDIRECT_LOCATION_HOME . '?qr=' . $qrID . '&ecode=QR_DISABLED';
			header($redirectURL);
			exit();
		}

		//Get some info for reporting
		$txtInternalID = $adDataRow['txt_internal_id'];
		$txtCollectEmail = $adDataRow['txt_collect_user_email'];
		$txtUserAgent = trim($_SERVER['HTTP_USER_AGENT']);
		$txtClientIP = trim($_SERVER['REMOTE_ADDR']);

		//See how the user entered the QR request, for reporting only
		if ($srcQR == USER_REQUESTED_QR_MANUAL)
		{
			$txtSource = USER_REQUESTED_QR_MANUAL;
		}
		else
		{
			$txtSource = USER_REQUESTED_QR_SCAN;
		}

		//If the AD is inactive, use the Advertiser's default URL if it has one, or show an error
		if ($adDataRow['txt_status_cd'] == AD_INACTIVE)
		{
			$redirect_url = $advertiserDataRow['txt_default_url'];
			if ($redirect_url == '')
			{
				//Write a USER_REQ record
				newViewRequest($advertiserID, $qrID, AD_INACTIVE, 'HOME_PAGE', $txtInternalID, $cuid, $txtUserAgent, $txtClientIP, $txtSource, $txtCollectEmail, USER_REQUESTED_EMAIL_FALSE);

				//The Advertiser has NO default URL, show an error
				$redirectURL = HTTP_REDIRECT_LOCATION_HOME . '?qr=' . $qrID . '&ecode=QR_INACTIVE';
				header($redirectURL);
				exit();
			}
			else
			{
				//Write a USER_REQ record
				newViewRequest($advertiserID, $qrID, AD_INACTIVE, $redirect_url, $txtInternalID, $cuid, $txtUserAgent, $txtClientIP, $txtSource, $txtCollectEmail, USER_REQUESTED_EMAIL_FALSE);

				//The Advertiser has a default URL, use it instead of the ad's url
				$redirect_url_location = "Location: http://" . $redirect_url;
				header($redirect_url_location);
				exit();
			}
		}

		//Finally, lets do a logical redirection!
		$redirect_url = $adDataRow['txt_url_page'];
		$redirect_url_location = "Location: http://" . $redirect_url;

		//Get the Ad data ready to show
		$qr_id = $adDataRow['id'];
		$advertiser_name = $advertiserDataRow['txt_company'];
		$public_desc = $adDataRow['txt_public_desc']; 
		$public_detail = $adDataRow['txt_public_detail'];

		//There are 3 options for emailing the user: DONT_ASK, OPTIONAL, REQUIRED
		if ($adDataRow['txt_collect_user_email'] == AD_COLLECT_EMAIL_OPTIONAL)
		{
			$bEmailOptional = TRUE;

			if ($btnYesEmail) //Post back from YES-EMAIL button
			{
				$validateEmailResult = validateEmail($email);
				if ($validateEmailResult->bSuccess)
				{
					//Update the cuid/email values, based on the posted email value
					$userReqResultObj = getUserTrackByEmail($email, $cuid);
					$userReqArray = $userReqResultObj->objResult;
					$cuid = $userReqArray['cuid'];
					$email = $userReqArray['email'];
					setcookie('cuid', $cuid, time()+60*60*24*365, '/', COOKIE_DOMAIN);

					//Write a USER_REQ record
					newViewRequest($advertiserID, $qrID, AD_ACTIVE, $redirect_url, $txtInternalID, $cuid, $txtUserAgent, $txtClientIP, $txtSource, AD_COLLECT_EMAIL_OPTIONAL, USER_REQUESTED_EMAIL_TRUE);

					header($redirect_url_location);
					sendEmailAdTouser($qr_id, $email, $advertiser_name, $public_desc, $public_detail, $redirect_url);
					exit();
				}
				else
				{
					//bad email address format
					$error_tag = $validateEmailResult->exStr;
				}
			}
			else if ($btnNoEmail) //Post back from NO-EMAIL button
			{
				//Update the cuid/email values, based on the cookie only
				$userReqResultObj = getUserTrackByCUID($cuid);
				$userReqArray = $userReqResultObj->objResult;
				$cuid = $userReqArray['cuid'];
				$email = $userReqArray['email'];
				setcookie('cuid', $cuid, time()+60*60*24*365, '/', COOKIE_DOMAIN);

				//Write a USER_REQ record
				newViewRequest($advertiserID, $qrID, AD_ACTIVE, $redirect_url, $txtInternalID, $cuid, $txtUserAgent, $txtClientIP, $txtSource, AD_COLLECT_EMAIL_OPTIONAL, USER_REQUESTED_EMAIL_FALSE);

				//Just redirect, no need to check email address
				header($redirect_url_location);
				exit();
			}
			else
			{
				//Dont forward, show the Pre-Ad, its the first time we are showing the screen
				//Get the user email
				$getUserResult = getUser($cuid);
				if ($getUserResult->bSuccess)
				{
					$userDataRow = $getUserResult->objResult;
					$email = $userDataRow['txt_email'];
				}
			}
		}
		else if ($adDataRow['txt_collect_user_email'] == AD_COLLECT_EMAIL_REQUIRE)
		{
			if ($btnYesEmail) //Post back from YES-EMAIL button
			{
				$validateEmailResult = validateEmail($email);
				if ($validateEmailResult->bSuccess)
				{
					//Update the cuid/email values, based on the posted email value
					$userReqResultObj = getUserTrackByEmail($email, $cuid);
					$userReqArray = $userReqResultObj->objResult;
					$cuid = $userReqArray['cuid'];
					$email = $userReqArray['email'];
					setcookie('cuid', $cuid, time()+60*60*24*365, '/', COOKIE_DOMAIN);

					//Write a USER_REQ record
					newViewRequest($advertiserID, $qrID, AD_ACTIVE, $redirect_url, $txtInternalID, $cuid, $txtUserAgent, $txtClientIP, $txtSource, AD_COLLECT_EMAIL_REQUIRE, USER_REQUESTED_EMAIL_TRUE);

					header($redirect_url_location);
					sendEmailAdTouser($qr_id, $email, $advertiser_name, $public_desc, $public_detail, $redirect_url);
					exit();
				}
				else
				{
					//bad email address format
					$error_tag = $validateEmailResult->exStr;
				}
			}
			else
			{
				//Dont forward, show the Pre-Ad, its the first time we are showing the screen
				//Check using the cookie only
                $getUserResult = getUser($cuid);
                if ($getUserResult->bSuccess)
                {
                    $userDataRow = $getUserResult->objResult;
                    $email = $userDataRow['txt_email'];
                }
			}
		}
		else if ($adDataRow['txt_collect_user_email'] == AD_COLLECT_EMAIL_DONT_ASK)
		{
			//Just redirect, no need to check email address
			//Update the cuid/email values, based on the cookie only
			$userReqResultObj = getUserTrackByCUID($cuid);
			$userReqArray = $userReqResultObj->objResult;
			$cuid = $userReqArray['cuid'];
			$email = $userReqArray['email'];
			setcookie('cuid', $cuid, time()+60*60*24*365, '/', COOKIE_DOMAIN);

			//Write a USER_REQ record
			newViewRequest($advertiserID, $qrID, AD_ACTIVE, $redirect_url, $txtInternalID, $cuid, $txtUserAgent, $txtClientIP, $txtSource, AD_COLLECT_EMAIL_DONT_ASK, USER_REQUESTED_EMAIL_FALSE);

			header($redirect_url_location);
			exit();
		}
		else
		{
			//This should not be hit, but in case...
			//So the options are just redirect, or show an error, I say just redirect for the user's benefit
			error_log("'qr_search.php > The adDataRow['txt_collect_user_email'] has an unexpected value for QRID: " . $qrID);
			header($redirect_url_location);
			exit();
		}
		
	}

}

catch (Exception $ex)
{
	//include "lib_error_handler.php";
	error_log('qr_search.php > There was an unexpected Exception');
	error_log($ex);
	$redirectURL = HTTP_REDIRECT_LOCATION_HOME . '?qr=' . $qrID . '&ecode=UNEXPECTED';
	error_log($redirectURL);
	header($redirectURL);
	exit();
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - QR Search</title>
</head>

<body>
<img alt="ClearQR" src="images/clearqr_icon.png">

<hr>

Opening the AD link now; Email you a copy?<br><br>
QR#: <b><?php echo $qr_id; ?></b><br>
Advertiser: <b><?php echo $advertiser_name; ?></b><br>
Description: <b><?php echo $public_desc; ?></b><br>
Details: <b><?php echo $public_detail; ?></b><br>

<br>

<form method="post" action="qr_search.php">
<input type="hidden" id="qr" name="qr" value="<?php echo $qr_id; ?>" />
<input type="hidden" id="src" name="src" value="<?php echo $txtSource; ?>" />
Email: <input size=30 name="email" value="<?php echo $email; ?>">
<input name="btn_yes_email" value="Email Me!" type="submit"><br>

<?php
	if ($bEmailOptional)
	{
		echo '<input name="btn_no_email" value="No thanks, just show me the ad" type="submit"><br>';
	}
?>

<span class="errortxt"><?php echo $error_tag; ?></span>
</form>

Or return to the <a href="/">ClearQR Home</a> page.<br><br>

<i>Send all inquiries to: <?php echo EMAIL_ADMIN; ?></i>

