To install dev:

-Create a subdomain 'devtest' to point to ~/public_html_devtest

-mkdir ~/logs
-mkdir ~/tmp_php_sessions_devtest
-mkdir ~/public_html_devtest

-Unzip code to the subdomain root (~/public_html_devtest)
-Run sql_create_tables.txt via phpMyAdmin (first time only)

-COnfigure .htaccess
-Configure php.ini
-Configure lib_app_constants.php
-Configure lib_db_conn.php
-Configure general.css

-Lock site with: chmod 500 public_html_devtest - (use 755 to turn on)
-Backup: tar -cvf clearqr_vxxx.tar public_html_devtest

------------------------------------

To install production:

-Copy public_html_devtest public_html_staging

-mkdir ~/logs
-mkdir ~/tmp_php_sessions

-COnfigure .htaccess
-Configure php.ini
-Configure lib_app_constants.php
-Configure lib_db_conn.php

-Setup server side config
-Setup Email Account (catch all, forward)

-rm public_html_old
-mv public_html public_html_old; mv public_html_staging public_html
-chmod 555 public_html

------------------------------------

Post Install:

-Make sure directory indexing is off
-Test: Check DB, Sessions, App Emails
-Check logs and session dirs after testing

