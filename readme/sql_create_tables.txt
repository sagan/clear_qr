-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 11, 2011 at 12:00 AM
-- Server version: 5.0.92
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `clearqrc_prod`
--

-- --------------------------------------------------------

--
-- Table structure for table `qr_ad`
--

CREATE TABLE IF NOT EXISTS `qr_ad` (
  `id` int(11) NOT NULL auto_increment,
  `fk_advertiser_id` int(11) NOT NULL,
  `txt_url_page` varchar(256) NOT NULL,
  `dt_create` datetime NOT NULL,
  `dt_update` datetime NOT NULL,
  `txt_status_cd` varchar(10) NOT NULL,
  `txt_internal_id` varchar(100) NOT NULL,
  `txt_internal_desc` varchar(256) NOT NULL,
  `txt_public_desc` varchar(256) NOT NULL,
  `txt_public_detail` varchar(256) NOT NULL,
  `txt_collect_user_email` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_advertiser_id` (`fk_advertiser_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Table structure for table `qr_advertiser`
--

CREATE TABLE IF NOT EXISTS `qr_advertiser` (
  `id` int(11) NOT NULL auto_increment,
  `txt_email` varchar(100) NOT NULL,
  `txt_password_hash` varchar(64) NOT NULL,
  `txt_company` varchar(100) NOT NULL,
  `txt_contact_name` varchar(100) NOT NULL,
  `txt_phone_number` varchar(20) NOT NULL,
  `dt_create` datetime NOT NULL,
  `dt_update` datetime NOT NULL,
  `txt_default_url` varchar(256) NOT NULL,
  `dt_last_login_attempt` datetime default NULL,
  `dt_last_login_success` datetime default NULL,
  `num_login_fail_count` int(11) NOT NULL,
  `txt_status_cd` varchar(10) NOT NULL,
  `txt_logo_icon_url` varchar(256) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `txt_email` (`txt_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Table structure for table `qr_tester`
--

CREATE TABLE IF NOT EXISTS `qr_tester` (
  `id` int(11) NOT NULL auto_increment,
  `str_a` varchar(20) default NULL,
  `num_b` int(11) default NULL,
  `dt_c` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Table structure for table `qr_user`
--

CREATE TABLE IF NOT EXISTS `qr_user` (
  `id` int(11) NOT NULL auto_increment,
  `txt_email` varchar(100) default NULL,
  `dt_create` datetime NOT NULL,
  `dt_last_request` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `txt_email` (`txt_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `qr_view_request`
--

CREATE TABLE IF NOT EXISTS `qr_view_request` (
  `id` int(11) NOT NULL auto_increment,
  `fk_advertiser_id` int(11) NOT NULL,
  `fk_ad_id` int(11) NOT NULL,
  `txt_ad_status_cd` varchar(10) NOT NULL,
  `txt_ad_url_page` varchar(256) NOT NULL,
  `txt_ad_internal_id` varchar(100) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `txt_client_user_agent` varchar(100) NOT NULL,
  `txt_client_ip` varchar(15) NOT NULL,
  `dt_create` datetime NOT NULL,
  `txt_source_cd` varchar(10) NOT NULL,
  `txt_collect_user_email` varchar(10) NOT NULL,
  `txt_user_requested_email` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_advertiser_id` (`fk_advertiser_id`),
  KEY `fk_ad_id` (`fk_ad_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;
