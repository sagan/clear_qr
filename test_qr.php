<?php

try
{
        include "lib_app_constants.php";
}

catch (Exception $ex)
{
        include "lib_error_handler.php";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=yes" />
  <link href="general.css" type= "text/css" rel="stylesheet" />
  <title>ClearQR - Test QR Reader</title>
</head>

<body>
<a href="/"><img alt="ClearQR" src="images/clearqr_icon.png"></a>

<hr>

<a href="/">Home</a> > Test QR Reader<br><br>

Now that you have <a href="get_qr_reader.php">installed</a> a QR Reader on your phone, lets give it a try.
<br>

<ul>
<li>On your PC computer, open this page.</li>
<li>From your mobile phone, start the QR Scanner/Reader app, and point the camera at this picture:</li>
</ul>

<img alt="ClearQR Test" src="images/qr_test_reader.png"></a>
<br><br>

After your phone recognizes the QR, proceed to open the URL link in your phone's browser. You will be redirected to a confirmation page.
<br><br>

<i>Send all inquiries to: <?php echo EMAIL_ADMIN; ?></i>

</body>
</html>
